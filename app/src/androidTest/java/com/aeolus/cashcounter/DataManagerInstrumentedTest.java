package com.aeolus.cashcounter;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.aeolus.cashcounter.Model.DTO.AccountDTO;
import com.aeolus.cashcounter.Model.DTO.AccountTypeDTO;
import com.aeolus.cashcounter.Model.DTO.CategoryDTO;
import com.aeolus.cashcounter.Model.DTO.DailyPaymentsDTO;
import com.aeolus.cashcounter.Model.DTO.PaymentDTO;
import com.aeolus.cashcounter.Model.DTO.PaymentTypeDTO;
import com.aeolus.cashcounter.Model.DataManager;

import junit.framework.Assert;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class DataManagerInstrumentedTest {
    private DataManager dataManager;
    List<CategoryDTO> list;

    @Before
    public void setUp() {
        dataManager = DataManager.getInstance(InstrumentationRegistry.getTargetContext());
    }

    @Test
    public void test() {

    }
}
