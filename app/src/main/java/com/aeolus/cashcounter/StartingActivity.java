package com.aeolus.cashcounter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.aeolus.cashcounter.View.MainPageActivity;

/**
 * Created by Yuriy on 20.05.2017.
 */

public class StartingActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = getSharedPreferences(getString(R.string.preferences), MODE_PRIVATE);
        super.onCreate(savedInstanceState);
        this.startActivity(new Intent(this, MainPageActivity.class));
    }
    @Override
    protected void onResume() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(getString(R.string.is_application_running), true);
        editor.apply();
        super.onResume();
    }
    @Override
    protected void onPause() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(getString(R.string.is_application_running), false);
        editor.apply();
        super.onPause();
    }
}
