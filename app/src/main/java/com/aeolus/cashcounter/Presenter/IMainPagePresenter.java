package com.aeolus.cashcounter.Presenter;

import android.app.Activity;
import android.location.Location;
import android.net.Uri;

import com.aeolus.cashcounter.Presenter.VO.AccountVO;
import com.aeolus.cashcounter.Presenter.VO.CurrencyVO;
import com.aeolus.cashcounter.Presenter.VO.DailyPaymentsVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentTypeVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentVO;
import com.aeolus.cashcounter.View.EnableLocationDialogFragment;
import com.google.android.gms.maps.model.LatLng;

import org.joda.time.LocalTime;

import java.util.ArrayList;

/**
 * Created by Yuriy on 10.05.2017.
 */

public interface IMainPagePresenter {
    String LOCATION_EXTRA_KEY = "location_extra_key";
    int FILE_CHOSEN_REQUEST = 15;
    String FILE_EXTENSION = ".ccp";

    AccountVO getCurrentAccount();

    void onAddNewExpenseButtonPressed();

    void onAddNewIncomeButtonPressed();

    void onNewPaymentAdded(PaymentVO payment);

    void onDeletePaymentPressed(PaymentVO p, DailyPaymentsVO dailyPaymentsVO);

    void onAllPaymentsAdded();

    void onAccountSelected(String accountName);

    void onAddNewAccountPressed();

    void onNewAccountAdded(AccountVO accountVO);

    void onNewCategoryTyped(String categoryName, PaymentTypeVO type);

    void onNewCategoryAdded(String categoryName, PaymentTypeVO type);

    void onShowAccountInformationPressed();

    void reloadList();

    Activity getActivity();

    void onDeleteCurrentAccountPressed();

    void onEnableLocationPressed(EnableLocationDialogFragment dialog);

    void onDisableLocationPressed();

    void onLocationUpdated(Location newLocation);

    void onLocationPermissionCheckFailed();

    Location getLastLocation();

    void onShowPaymentOnMapPressed(LatLng location);

    void onCalculateDebtsButtonPressed();

    void onEvenlyDistributionPressed(boolean evenly);

    void processDebtsPaymentsSelection(String name, ArrayList<PaymentVO> payments);

    void onCancelSelectionPressed();

    void onAboutPressed();

    void onManageNotificationsPressed();

    void onEnableNotificationsPressed(LocalTime time);

    void onDisableNotificationsPressed();

    void onImportPressed();

    void onExportPressed();

    void onFileSelected(Uri uri);

    void onPaymentsForExportingSelected(ArrayList<PaymentVO> payments);

    void onEnableGpsInSettingsPressed();

    boolean isGpsEnabled();

    void onActivityResume();

    void onActivityPause();

    void onManageLocationPressed();

    void onStatisticsPressed();

    void onCurrencyVariantChosen(CurrencyVO currency);

    void onCurrencyVariantConvertToUAHChosen();
}
