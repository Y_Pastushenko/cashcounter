package com.aeolus.cashcounter.Presenter.VO;

import android.os.Parcel;
import android.os.Parcelable;

import org.joda.time.DateTime;

/**
 * Created by Yuriy on 09.05.2017.
 */

public class PaymentVO implements Cloneable, Parcelable {

    private long id;
    private String accountName;
    private String payerName;
    private double amount;
    private CurrencyVO currency;
    private PaymentTypeVO type;
    private String category;
    private String comment;
    private DateTime dt;
    private double latitude;
    private double longitude;

    public PaymentVO(String accountName,
                     String payerName,
                     PaymentTypeVO type,
                     DateTime dt,
                     double latitude,
                     double longitude,
                     CurrencyVO currency,
                     double amount,
                     String category,
                     String comment) {
        this.accountName = accountName;
        this.payerName = payerName;
        this.type = type;
        this.dt = dt;
        this.latitude = latitude;
        this.longitude = longitude;
        this.currency = currency;
        this.amount = amount;
        this.category = category;
        this.comment = comment;
    }

    public PaymentVO(Parcel in) {
        id = in.readLong();
        accountName = in.readString();
        payerName = in.readString();
        amount = in.readDouble();
        currency = (CurrencyVO) in.readSerializable();
        type = (PaymentTypeVO) in.readSerializable();
        category = in.readString();
        comment = in.readString();
        dt = new DateTime(in.readLong());
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        public PaymentVO createFromParcel(Parcel in) {
            return new PaymentVO(in);
        }

        public PaymentVO[] newArray(int size) {
            return new PaymentVO[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(accountName);
        dest.writeString(payerName);
        dest.writeDouble(amount);
        dest.writeSerializable(currency);
        dest.writeSerializable(type);
        dest.writeString(category);
        dest.writeString(comment);
        dest.writeLong(dt.getMillis());
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }


    public String getAccount() {
        return accountName;
    }

    public void setAccount(String accountName) {
        this.accountName = accountName;
    }

    public PaymentTypeVO getType() {
        return type;
    }

    public void setType(PaymentTypeVO type) {
        this.type = type;
    }

    public DateTime getDt() {
        return dt;
    }

    public void setDt(DateTime dt) {
        this.dt = dt;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public CurrencyVO getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyVO currency) {
        this.currency = currency;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
