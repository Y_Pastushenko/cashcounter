package com.aeolus.cashcounter.Presenter.BL;

import com.aeolus.cashcounter.Model.DTO.PaymentDTO;

import java.util.List;

/**
 * Created by Yuriy on 17.09.2017.
 */

public class PaymentsAndAssignedPayers {
    private List<PaymentDTO> payments;
    private List<String> payers;

    public PaymentsAndAssignedPayers(List<PaymentDTO> payments, List<String> payers) {
        this.payments = payments;
        this.payers = payers;
    }

    public void addPayment(PaymentDTO p) {
        payments.add(p);
    }

    public void addPayer(String p) {
        payers.add(p);
    }

    public List<PaymentDTO> getPayments() {
        return payments;
    }

    public void setPayments(List<PaymentDTO> payments) {
        this.payments = payments;
    }

    public List<String> getPayers() {
        return payers;
    }

    public void setPayers(List<String> payers) {
        this.payers = payers;
    }
}
