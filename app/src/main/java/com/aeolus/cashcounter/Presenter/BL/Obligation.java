package com.aeolus.cashcounter.Presenter.BL;

import android.support.annotation.NonNull;

/**
 * Created by Yuriy on 07.09.2017.
 */

public class Obligation implements Comparable {
    private String debtor;
    private String creditor;
    private double amount;
    private String currency;

    public Obligation(String debtor, String creditor, double amount, String currency) {
        this.debtor = debtor;
        this.creditor = creditor;
        this.amount = amount;
        this.currency = currency;
    }

    public String getDebtor() {
        return debtor;
    }

    public void setDebtor(String debtor) {
        this.debtor = debtor;
    }

    public String getCreditor() {
        return creditor;
    }

    public void setCreditor(String creditor) {
        this.creditor = creditor;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        Obligation obligation = (Obligation) o;
        return  this.getDebtor().compareTo(obligation.getDebtor());
    }
}
