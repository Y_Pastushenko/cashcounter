package com.aeolus.cashcounter.Presenter.VO;

/**
 * Created by Yuriy on 06.05.2017.
 */

public class AccountVO {
    private String accountName;
    private AccountTypeVO accountType;

    public AccountVO(String accountName, AccountTypeVO type) {
        this.accountName = accountName;
        this.accountType = type;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public AccountTypeVO getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountTypeVO accountType) {
        this.accountType = accountType;
    }


}
