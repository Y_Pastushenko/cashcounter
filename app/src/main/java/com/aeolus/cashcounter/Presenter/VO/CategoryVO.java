package com.aeolus.cashcounter.Presenter.VO;


/**
 * Created by Yuriy on 06.05.2017.
 */

public class CategoryVO {
    private int id;
    private String accountName;
    private String categoryName;
    private PaymentTypeVO type;

    public CategoryVO(int id, String accountName, String categoryName, PaymentTypeVO type) {
        this.id = id;
        this.categoryName = categoryName;
        this.type = type;
    }

    public int getCategoryId() {
        return id;
    }

    public void setCategoryId(int id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public PaymentTypeVO getType() {
        return type;
    }

    public void setType(PaymentTypeVO type) {
        this.type = type;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
