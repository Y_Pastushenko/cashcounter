package com.aeolus.cashcounter.Presenter.VO;

/**
 * Created by Yuriy on 11/05/2017.
 */

public enum PaymentTypeVO {
    INCOME, EXPENSE
}
