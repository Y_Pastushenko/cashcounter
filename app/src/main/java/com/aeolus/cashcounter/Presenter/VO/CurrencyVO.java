package com.aeolus.cashcounter.Presenter.VO;

/**
 * Created by Yuriy on 07.08.2017.
 */

public enum CurrencyVO {
    UAH, USD, EUR
}
