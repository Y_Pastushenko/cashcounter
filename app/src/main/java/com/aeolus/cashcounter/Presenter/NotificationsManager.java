package com.aeolus.cashcounter.Presenter;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.aeolus.cashcounter.R;
import com.aeolus.cashcounter.StartingActivity;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

/**
 * Created by Yuriy on 18/02/2017.
 */

public class NotificationsManager extends BroadcastReceiver implements INotificationsManager {

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preferences), Context.MODE_PRIVATE);
        Boolean b = sharedPref.getBoolean(context.getString(R.string.is_application_running), false);
        if (!b) {
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(context.getResources().getString(R.string.notification_title))
                            .setContentText(context.getResources().getString(R.string.notification_text))
                            .setAutoCancel(true)
                            .setSound(alarmSound);
            Intent resultIntent = new Intent(context, StartingActivity.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(StartingActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(1, mBuilder.build());
        }
    }

    @Override
    public void enableNotifications(Context context, long time, long interval) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, NotificationsManager.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        LocalDate date = new LocalDate();
        long setTime = new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(), 0, 0).getMillis() + time;
        am.setRepeating(AlarmManager.RTC_WAKEUP, setTime, interval, pi);
    }

    @Override
    public void disableNotifications(Context context) {
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, NotificationsManager.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        am.cancel(pi);
        pi.cancel();
    }

    @Override
    public boolean checkIfNotificationsEnabled(Context context) {
        Intent i = new Intent(context, NotificationsManager.class);
        return (PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_NO_CREATE) != null);
    }
}
