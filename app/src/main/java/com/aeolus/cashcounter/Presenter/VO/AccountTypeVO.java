package com.aeolus.cashcounter.Presenter.VO;

/**
 * Created by Kostya on 26.09.2017.
 */

public enum AccountTypeVO {
    PERSONAL, GROUP
}
