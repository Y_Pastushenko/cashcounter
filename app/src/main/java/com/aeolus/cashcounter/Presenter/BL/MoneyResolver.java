package com.aeolus.cashcounter.Presenter.BL;

import com.aeolus.cashcounter.Model.DTO.PaymentDTO;
import com.aeolus.cashcounter.Presenter.VO.CurrencyVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentVO;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.aeolus.cashcounter.Presenter.BL.NumericTools.doubleEquals;

/**
 * Created by Yuriy on 07.09.2017.
 */

public class MoneyResolver {
    private String maxDebtorName;
    private String maxCreditorName;
    private String currency;

    public List<Obligation> calculateEvenlyDistributedPayments(List<PaymentsAndAssignedPayers> paymentsAndPayersList, boolean round) {
        HashMap<String, Double> debts = new HashMap<>();
        HashMap<String, Double> credits = new HashMap<>();
        for (PaymentsAndAssignedPayers pp : paymentsAndPayersList) {
            List<PaymentDTO> payments = pp.getPayments();
            if (!payments.isEmpty())
                currency = payments.get(0).getCurrency().toString();
            List<String> payers = Stream.of(pp.getPayers()).distinct().collect(Collectors.toList());
            double generalSum = Stream.of(payments).mapToDouble(p -> p.getAmount()).sum();
            double sumPerPerson = generalSum / payers.size();
            for (String payer : payers)
                debts.put(payer, debts.containsKey(payer) ? (sumPerPerson + debts.get(payer)) : sumPerPerson);
            for (PaymentDTO p : payments) {
                credits.put(p.getPayerName(), credits.containsKey(p.getPayerName()) ? (p.getAmount() + credits.get(p.getPayerName())) : p.getAmount());
            }
        }
        return resolveDebts(debts, credits, round);
    }

    public List<Obligation> calculateNotEvenlyDistributedPayments(HashMap<String, ArrayList<PaymentVO>> selectedPayments, ArrayList<String> payers, boolean round) {
        HashMap<String, Double> debts = new HashMap<>();
        HashMap<String, Double> credits = new HashMap<>();
        List<Obligation> obligations = new ArrayList<>();
        for (CurrencyVO currencyVO : CurrencyVO.values()) {
            ArrayList<Long> paymentIDs;
            ArrayList<PaymentVO> payments = new ArrayList<>();
            HashMap<String, ArrayList<PaymentVO>> selectedPaymentsForCurrency = new HashMap<>();
            for (Map.Entry<String, ArrayList<PaymentVO>> e : selectedPayments.entrySet()) {
                List<PaymentVO> paymentsInCurrency = e.getValue();
                paymentsInCurrency = Stream.of(paymentsInCurrency).filter(p -> p.getCurrency().equals(currencyVO)).toList();
                selectedPaymentsForCurrency.put(e.getKey(), new ArrayList<>(paymentsInCurrency));
                payments.addAll(paymentsInCurrency);
            }
            if (payments.isEmpty())
                continue;
            paymentIDs = new ArrayList<>(Stream.of(payments).map(PaymentVO::getId).distinct().collect(Collectors.toList()));
            ArrayList<PaymentVO> tPayments = new ArrayList<>();
            for (long id : paymentIDs)
                for (PaymentVO p : payments)
                    if (p.getId() == id) {
                        tPayments.add(p);
                        break;
                    }
            payments = tPayments;
            payers = new ArrayList<>(Stream.of(payers).distinct().collect(Collectors.toList()));
            Collections.sort(paymentIDs);
            currency = payments.get(0).getCurrency().toString();
            int[][] paymentsMatrix = new int[payers.size()][paymentIDs.size()];
            for (Map.Entry<String, ArrayList<PaymentVO>> e : selectedPaymentsForCurrency.entrySet())
                for (PaymentVO p : e.getValue()) {
                    paymentsMatrix[payers.indexOf(e.getKey())][paymentIDs.indexOf(p.getId())] = 1;
                }
            debts = new HashMap<>();
            credits = new HashMap<>();
            for (int iPayment = 0; iPayment < paymentIDs.size(); iPayment++) {
                ArrayList<String> payersNames = new ArrayList<>();
                for (int iPayer = 0; iPayer < payers.size(); iPayer++) {
                    if (paymentsMatrix[iPayer][iPayment] == 1)
                        payersNames.add(payers.get(iPayer));
                }
                PaymentVO payment = payments.get(0);
                for (PaymentVO p : payments)
                    if (p.getId() == paymentIDs.get(iPayment)) {
                        payment = p;
                        break;
                    }
                double debtResult = payment.getAmount() / payersNames.size();
                for (String name : payersNames) {
                    if (debts.containsKey(name))
                        debts.put(name, debts.get(name) + debtResult);
                    else
                        debts.put(name, debtResult);
                }
                if (credits.containsKey(payment.getPayerName()))
                    credits.put(payment.getPayerName(), credits.get(payment.getPayerName()) + payment.getAmount());
                else
                    credits.put(payment.getPayerName(), payment.getAmount());
            }
            obligations.addAll(resolveDebts(debts, credits, round));
        }
        return obligations;
    }

    private List<Obligation> resolveDebts(HashMap<String, Double> debts, HashMap<String, Double> credits, boolean round) {
        HashMap<String, Double> debtors = new HashMap<>();
        HashMap<String, Double> creditors = new HashMap<>();
        for (Map.Entry<String, Double> c : credits.entrySet()) {
            if (!debts.containsKey(c.getKey())) {
                creditors.put(c.getKey(), creditors.containsKey(c.getKey()) ? creditors.get(c.getKey()) + c.getValue() : c.getValue());
            } else {
                if (doubleEquals(debts.get(c.getKey()), c.getValue())) {
                    debts.remove(c.getKey());
                } else {
                    if (c.getValue() > debts.get(c.getKey())) {
                        creditors.put(c.getKey(), creditors.containsKey(c.getKey()) ?
                                creditors.get(c.getKey()) + c.getValue() - debts.get(c.getKey()) :
                                c.getValue() - debts.get(c.getKey()));
                        debts.remove(c.getKey());
                    } else {
                        debtors.put(c.getKey(), debtors.containsKey(c.getKey()) ?
                                debtors.get(c.getKey()) + debts.get(c.getKey()) - c.getValue() :
                                debts.get(c.getKey()) - c.getValue());
                        debts.remove(c.getKey());
                    }
                }
            }
        }
        credits.clear();
        for (Map.Entry<String, Double> d : debts.entrySet()) {
            if (!credits.containsKey(d.getKey())) {
                debtors.put(d.getKey(), debtors.containsKey(d.getKey()) ? debtors.get(d.getKey()) + d.getValue() : d.getValue());
            } else {
                if (doubleEquals(credits.get(d.getKey()), d.getValue())) {
                    credits.remove(d.getKey());
                } else {
                    if (d.getValue() > credits.get(d.getKey())) {
                        debtors.put(d.getKey(), debtors.containsKey(d.getKey()) ?
                                debtors.get(d.getKey()) + d.getValue() - credits.get(d.getKey()) :
                                d.getValue() - credits.get(d.getKey()));
                        credits.remove(d.getKey());
                    } else {
                        creditors.put(d.getKey(), creditors.containsKey(d.getKey()) ?
                                creditors.get(d.getKey()) + credits.get(d.getKey()) - d.getValue() :
                                credits.get(d.getKey()) - d.getValue());
                        credits.remove(d.getKey());
                    }
                }
            }
        }
        debts.clear();

        ArrayList<Obligation> obligations = new ArrayList<>();
        while (true) {
            double maxDebtorValue = 0;
            double maxCreditorValue = 0;
            for (Map.Entry<String, Double> e : debtors.entrySet()) {
                if (e.getValue() > maxDebtorValue) {
                    maxDebtorValue = e.getValue();
                    maxDebtorName = e.getKey();
                }
            }
            for (Map.Entry<String, Double> e : creditors.entrySet()) {
                if (e.getValue() > maxCreditorValue) {
                    maxCreditorValue = e.getValue();
                    maxCreditorName = e.getKey();
                }
            }
            if (doubleEquals(maxCreditorValue, maxDebtorValue, 0.001)) {
                obligations.add(new Obligation(maxDebtorName, maxCreditorName, round ? NumericTools.round(maxCreditorValue, 2) : maxCreditorValue, currency));
                creditors.remove(maxCreditorName);
                debtors.remove(maxDebtorName);
            } else {
                if (maxCreditorValue > maxDebtorValue) {
                    obligations.add(new Obligation(maxDebtorName, maxCreditorName, round ? NumericTools.round(maxDebtorValue, 2) : maxDebtorValue, currency));
                    creditors.put(maxCreditorName, maxCreditorValue - maxDebtorValue);
                    debtors.remove(maxDebtorName);
                } else {
                    obligations.add(new Obligation(maxDebtorName, maxCreditorName, round ? NumericTools.round(maxCreditorValue, 2) : maxCreditorValue, currency));
                    debtors.put(maxDebtorName, maxDebtorValue - maxCreditorValue);
                    creditors.remove(maxCreditorName);
                }
            }
            if (debtors.isEmpty() || creditors.isEmpty())
                break;
        }
        for (Obligation o : obligations)
            if (doubleEquals(o.getAmount(), 0))
                obligations.remove(o);
        Collections.sort(obligations);
        return obligations;
    }
}

