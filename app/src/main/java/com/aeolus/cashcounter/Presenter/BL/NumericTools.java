package com.aeolus.cashcounter.Presenter.BL;

import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by Yuriy on 05/04/2017.
 */
public class NumericTools {
    public static double round(double value, int scale) {
        return Math.round(value * Math.pow(10, scale)) / Math.pow(10, scale);
    }

    public static int nextInt() {
        return new Random().nextInt();
    }

    public static int nextInt(int upperBound) {
        return new Random().nextInt(upperBound);
    }

    public static int nextInt(int lowerBound, int upperBound) {
        return new Random().nextInt(upperBound - lowerBound) + lowerBound;
    }

    public static int getLengthOfNumber(int number) {
        String s = Integer.toString(number);
        return s.length();
    }

    public static boolean doubleEquals(double left, double right, double eps) {
        return (Math.abs(left - right) <= eps);
    }

    public static boolean doubleEquals(double left, double right) {
        double eps = 0.001;
        return (Math.abs(left - right) <= eps);
    }

    public static boolean contains(long[] array, long value) {
        if (array.length == 0)
            return false;
        for (long l : array)
            if (l == value)
                return true;
        return false;
    }
}
