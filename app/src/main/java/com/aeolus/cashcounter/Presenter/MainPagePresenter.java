package com.aeolus.cashcounter.Presenter;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteConstraintException;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;

import com.aeolus.cashcounter.Model.DTO.AccountDTO;
import com.aeolus.cashcounter.Model.DTO.AccountTypeDTO;
import com.aeolus.cashcounter.Model.DTO.CategoryDTO;
import com.aeolus.cashcounter.Model.DTO.CurrencyDTO;
import com.aeolus.cashcounter.Model.DTO.DailyPaymentsDTO;
import com.aeolus.cashcounter.Model.DTO.PaymentDTO;
import com.aeolus.cashcounter.Model.DTO.PaymentForSerializing;
import com.aeolus.cashcounter.Model.DTO.PaymentTypeDTO;
import com.aeolus.cashcounter.Model.DataManager;
import com.aeolus.cashcounter.Model.IDataManger;
import com.aeolus.cashcounter.Model.Net.NetManager;
import com.aeolus.cashcounter.Presenter.BL.MoneyResolver;
import com.aeolus.cashcounter.Presenter.BL.NumericTools;
import com.aeolus.cashcounter.Presenter.BL.Obligation;
import com.aeolus.cashcounter.Presenter.BL.PaymentsAndAssignedPayers;
import com.aeolus.cashcounter.Presenter.VO.AccountTypeVO;
import com.aeolus.cashcounter.Presenter.VO.AccountVO;
import com.aeolus.cashcounter.Presenter.VO.CategoryVO;
import com.aeolus.cashcounter.Presenter.VO.CurrencyVO;
import com.aeolus.cashcounter.Presenter.VO.DailyPaymentsVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentTypeVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentVO;
import com.aeolus.cashcounter.Presenter.VO.SelectionType;
import com.aeolus.cashcounter.R;
import com.aeolus.cashcounter.View.EnableLocationDialogFragment;
import com.aeolus.cashcounter.View.IView;
import com.aeolus.cashcounter.View.MapsActivity;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.joda.time.LocalTime;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

import static android.content.Context.MODE_PRIVATE;
import static android.os.ParcelFileDescriptor.MODE_WORLD_READABLE;

/**
 * Created by Yuriy on 19.05.2017.
 */

public class MainPagePresenter implements IMainPagePresenter {
private static MainPagePresenter instance = new MainPagePresenter();

    //<editor-fold desc="Fields">
    private static IDataManger dataManager;
    private static IView mainPage;
    private ArrayList<PaymentDTO> paymentsForAccount = new ArrayList<>();
    private ArrayList<CategoryDTO> incomeCategoriesForAccount;
    private ArrayList<CategoryDTO> expenseCategoriesForAccount;
    private ArrayList<AccountDTO> accountDTOs;
    private final DailyPaymentsDTO dailyPayments = new DailyPaymentsDTO();
    private AccountDTO currentAccount;
    private static boolean listIsEmpty = true;
    private Location lastLocation = null;
    private static HashMap<String, ArrayList<PaymentVO>> selectedPayments;
    private List<String> payersForAccount;
    private static List<String> payersForSelection;
    private static final float MIN_RATIO_FOR_CATEGORY = 0.01f;
    private INotificationsManager notificationsManager;
    private static final long NOTIFICATIONS_INTERVAL = 86400000;
    private HashMap<String, Float> statisticsExpenseRatios;
    private HashMap<String, Double> statisticsExpenseCategoriesValues;
    private HashMap<String, Float> statisticsIncomeRatios;
    private HashMap<String, Double> statisticsIncomeCategoriesValues;
    //</editor-fold>

    private BroadcastReceiver locationSwitchStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
                final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    onDisableLocationPressed();
                } else
                    mainPage.showLocationEnableDialog(dataManager.getLocationStatus());
            }
        }
    };

    private MainPagePresenter(){}

    public static MainPagePresenter getInstance(){
        return instance;
    }

    public static void setView(IView mainPage) {
        MainPagePresenter.mainPage = mainPage;
        MainPagePresenter.dataManager = DataManager.getInstance(mainPage.getActivity());
    }

    public void init() {
        dataManager.setPresenter(this);
        notificationsManager = new NotificationsManager();
        updateAccountsList();
        if (!accountDTOs.isEmpty()) {
            mainPage.inflateAccounts(accountsDtoToVO(accountDTOs));
        } else {
            createDefaultAccount();
            init();
        }
        String lastAccountName = dataManager.getLastAccountName();
        if (lastAccountName.length() != 0 && Stream.of(accountDTOs).map(AccountDTO::getAccountName).toList().contains(lastAccountName))
            mainPage.selectAccount(lastAccountName);
    }

    @Override
    public Activity getActivity() {
        return mainPage.getActivity();
    }

    @Override
    public void onActivityResume() {
        SharedPreferences.Editor editor = getActivity().getSharedPreferences(getActivity().getString(R.string.preferences), MODE_PRIVATE).edit();
        editor.putBoolean(getActivity().getString(R.string.is_application_running), true);
        editor.apply();
        getActivity().registerReceiver(locationSwitchStateReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Boolean enabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (dataManager.getLocationStatus() != enabled)
            if (!enabled)
                mainPage.showTurnGpsOnDialog();
        if (dataManager.getLocationStatus()) {
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                enableLocation();
        }
    }

    @Override
    public void onActivityPause() {
        SharedPreferences.Editor editor = getActivity().getSharedPreferences(getActivity().getString(R.string.preferences), MODE_PRIVATE).edit();
        editor.putBoolean(getActivity().getString(R.string.is_application_running), false);
        editor.apply();
        getActivity().unregisterReceiver(locationSwitchStateReceiver);
    }

    @Override
    public void onAboutPressed() {
        mainPage.showAbout();
    }


    //<editor-fold desc="Notifications">
    @Override
    public void onManageNotificationsPressed() {
        mainPage.showNotificationsSettingsDialog(notificationsManager.checkIfNotificationsEnabled(getActivity()), dataManager.getNotificationTime());
    }

    @Override
    public void onEnableNotificationsPressed(LocalTime time) {
        notificationsManager.enableNotifications(getActivity(), time.getMillisOfDay(), NOTIFICATIONS_INTERVAL);
        dataManager.saveNotificationTime(time);
        mainPage.makeMessage(getActivity().getResources().getString(R.string.notification_enabled_message) + " " + time.toString("HH:mm"));
    }

    @Override
    public void onDisableNotificationsPressed() {
        notificationsManager.disableNotifications(getActivity());
        mainPage.makeMessage(getActivity().getResources().getString(R.string.notification_disabled_message));
    }
    //</editor-fold>

    //<editor-fold desc="Import & export">
    @Override
    public void onExportPressed() {
        mainPage.setSelectionModOn(getActivity().getResources().getString(R.string.message_for_selection_payments_for_export), SelectionType.FOR_EXPORT);
    }

    @Override
    public void onPaymentsForExportingSelected(ArrayList<PaymentVO> payments) {
        ArrayList<PaymentDTO> dtos = paymentsVoToDTO(payments);
        List<PaymentForSerializing> paymentForSerializing = Stream.of(dtos).map(PaymentDTO::getPaymentForSerializing).toList();
        String jsonString = new Gson().toJson(paymentForSerializing);
        String fileName = "payments" + FILE_EXTENSION;

        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            File root = getActivity().getExternalCacheDir();
            File dir = new File(root.getAbsolutePath() + "/CashCounter");
            dir.mkdirs();
            File file = new File(dir, fileName);
            try {
                FileOutputStream f = new FileOutputStream(file);
                PrintWriter pw = new PrintWriter(f);
                pw.println(jsonString);
                pw.flush();
                pw.close();
                f.close();
            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            }
        } else {
            FileOutputStream fOut = null;
            try {
                fOut = getActivity().openFileOutput(fileName, MODE_WORLD_READABLE);
            } catch (FileNotFoundException e) {
                return;
            }
            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            try {
                osw.write(jsonString);
                osw.flush();
                osw.close();
            } catch (IOException e) {
                return;
            }
        }
        Uri uri = Uri.fromFile(getActivity().getFileStreamPath(fileName));
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.setType("multipart/encrypted");
        getActivity().startActivity(Intent.createChooser(shareIntent, "Send by..."));
    }

    @Override
    public void onImportPressed() {
        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        getActivity().startActivityForResult(
                Intent.createChooser(intent, "Select " + FILE_EXTENSION + " file"), FILE_CHOSEN_REQUEST);
        mainPage.makeMessage("Choose " + FILE_EXTENSION + " file!");
    }

    @Override
    public void onFileSelected(Uri uri) {
        try {
            ContentResolver cr = getActivity().getContentResolver();
            String jsonString = "";
            try {
                Scanner s = new Scanner(cr.openInputStream(uri)).useDelimiter("\\A");
                jsonString = s.hasNext() ? s.next() : "";
            } catch (FileNotFoundException e) {
            }
            if (jsonString.length() == 0)
                return;
            PaymentForSerializing[] payments = new Gson().fromJson(jsonString, PaymentForSerializing[].class);
            for (PaymentForSerializing p : payments) {
                p.setAccountName(currentAccount.getAccountName());
                dataManager.savePayment(new PaymentDTO(p));
            }
            reloadList();
        } catch (JsonSyntaxException e) {
            mainPage.makeMessage(getActivity().getResources().getString(R.string.invalid_file_message));
        }
    }


    //</editor-fold>

    //<editor-fold desc="Payments">
    @Override
    public void onAddNewIncomeButtonPressed() {
        mainPage.showAddPaymentDialog(null, currentAccount.getAccountName(), PaymentTypeVO.INCOME, categoriesDtoToVO(incomeCategoriesForAccount), new ArrayList<>(payersForAccount));
    }

    @Override
    public void onAddNewExpenseButtonPressed() {
        mainPage.showAddPaymentDialog(null, currentAccount.getAccountName(), PaymentTypeVO.EXPENSE, categoriesDtoToVO(expenseCategoriesForAccount), new ArrayList<>(payersForAccount));
    }

    @Override
    public void onNewPaymentAdded(PaymentVO payment) {
        dataManager.savePayment(voToDTO(payment));
        mainPage.makeMessage(getActivity().getResources().getString(R.string.payment_added_message));
//        if (payment.getType().equals(PaymentTypeVO.INCOME))
//            onAddNewIncomeButtonPressed();
//        else
//            onAddNewExpenseButtonPressed();
    }

    @Override
    public void onAllPaymentsAdded() {
        reloadList();
    }

    @Override
    public void onDeletePaymentPressed(PaymentVO p, DailyPaymentsVO dailyPaymentsVO) {
        dataManager.deletePayment(voToDTO(p));
        mainPage.deletePaymentFromList(p, dailyPaymentsVO);
        if (mainPage.getListCount() == 0) {
            mainPage.clearList();
            mainPage.pinNoPaymentsMessage(true);
            return;
        }
        reloadList();
        mainPage.scrollTo(dailyPaymentsVO);
    }

    private void updatePayers() {
        payersForAccount = Stream.of(paymentsForAccount).map(PaymentDTO::getPayerName).distinct().collect(Collectors.toList());
    }

    @Override
    public void reloadList() {
        mainPage.clearList();
        paymentsForAccount.clear();
        Observer<PaymentDTO> observer = new Observer<PaymentDTO>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull PaymentDTO paymentDTO) {
                paymentsForAccount.add(paymentDTO);
                listIsEmpty = false;
                try {
                    dailyPayments.add(paymentDTO);
                } catch (DailyPaymentsDTO.IncorrectDateInsertedException e) {
                    mainPage.appendToList(dtoToVO(dailyPayments));
                    dailyPayments.clear();
                    try {
                        dailyPayments.add(paymentDTO);
                    } catch (DailyPaymentsDTO.IncorrectDateInsertedException ex) {
                        throw new Error();
                    }
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                throw new Error();
            }

            @Override
            public void onComplete() {
                if (!listIsEmpty) {
                    mainPage.pinNoPaymentsMessage(false);
                    mainPage.appendToList(dtoToVO(dailyPayments));
                    updatePayers();
                    dailyPayments.clear();
                    listIsEmpty = true;
                } else {
                    mainPage.pinNoPaymentsMessage(true);
                }
            }
        };
        dataManager.getPayments(currentAccount.getAccountName()).subscribe(observer);
    }

    //</editor-fold>

    //<editor-fold desc="Account">

    private void createDefaultAccount() {
        AccountDTO account = new AccountDTO("Default", AccountTypeDTO.PERSONAL);
        dataManager.saveAccount(account);
        addDefaultCategories(account);
    }

    @Override
    public void onAddNewAccountPressed() {
        mainPage.showAddNewAccountDialog(null);
    }

    @Override
    public void onNewAccountAdded(AccountVO accountVO) {
        try {
            dataManager.saveAccount(voToDTO(accountVO));
        } catch (SQLiteConstraintException e) {
            mainPage.makeMessage(getActivity().getResources().getString(R.string.account_already_exists_message));
        }
        updateAccountsList();
        mainPage.inflateAccounts(accountsDtoToVO(accountDTOs));
        mainPage.selectAccount(accountVO.getAccountName());
    }

    @Override
    public void onAccountSelected(String accountName) {
        for (AccountDTO a : accountDTOs)
            if (a.getAccountName().equals(accountName))
                currentAccount = a;
        if (currentAccount == null)
            currentAccount = accountDTOs.get(0);
        paymentsForAccount.clear();
        if (currentAccount.getAccountType() == AccountTypeDTO.PERSONAL)
            mainPage.setOnPurseMod();
        else
            mainPage.setOnGroupCostsMod();
        reloadList();
        updateCategories();
        updatePayers();
        dataManager.saveLastAccountName(currentAccount.getAccountName());
    }

    @Override
    public AccountVO getCurrentAccount() {
        return dtoToVO(currentAccount);
    }

    @Override
    public void onShowAccountInformationPressed() {
        String totalIncome = "";
        String totalExpense = "";
        String summary = "";
        for (CurrencyDTO currency : CurrencyDTO.values()) {
            double currentCurrencyIncome = 0;
            double currentCurrencyExpense = 0;
            for (PaymentDTO p : paymentsForAccount)
                if (p.getCurrency().equals(currency))
                    if (p.getType() == PaymentTypeDTO.INCOME)
                        currentCurrencyIncome += p.getAmount();
                    else
                        currentCurrencyExpense += p.getAmount();
            if (currentCurrencyIncome == 0 && currentCurrencyExpense == 0)
                continue;
            if (currentCurrencyIncome != 0)
                totalIncome += "+" + currentCurrencyIncome + " " + currency + "\n";
            if (currentCurrencyExpense != 0)
                totalExpense += "-" + currentCurrencyExpense + " " + currency + "\n";
            summary += "" + (currentCurrencyIncome - currentCurrencyExpense) + " " + currency + "\n";
        }
        mainPage.showAccountInformation(currentAccount.getAccountName(), totalIncome, totalExpense, summary);
    }

    private void updateAccountsList() {
        dataManager.getAccounts().subscribe(a -> accountDTOs = a);
    }

    @Override
    public void onDeleteCurrentAccountPressed() {
        dataManager.deletePayments(currentAccount);
        dataManager.deleteAccount(currentAccount);
        dataManager.deleteCategoriesByAccount(currentAccount);
        updateAccountsList();
        if (accountDTOs.isEmpty())
            createDefaultAccount();
        updateAccountsList();
        currentAccount = accountDTOs.get(0);
        mainPage.inflateAccounts(accountsDtoToVO(accountDTOs));
        mainPage.selectAccount(currentAccount.getAccountName());
    }

    //</editor-fold>

    //<editor-fold desc="Location">

    @Override
    public boolean isGpsEnabled() {
        return (lastLocation != null);
    }

    @Override
    public void onEnableLocationPressed(EnableLocationDialogFragment dialog) {
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            mainPage.showTurnGpsOnDialog();
        else {
            enableLocation();
            dialog.setChecked();
        }
    }

    void enableLocation() {
        dataManager.enableLocation(true);
        dataManager.saveLocationStatus(true);
    }

    @Override
    public void onDisableLocationPressed() {
        lastLocation = null;
        dataManager.enableLocation(false);
        dataManager.saveLocationStatus(false);
    }

    @Override
    public void onEnableGpsInSettingsPressed() {
        getActivity().startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }

    @Override
    public void onLocationUpdated(Location newLocation) {
        if (lastLocation == null)
            mainPage.notifyLocationAvailable(true);
        lastLocation = newLocation;
    }

    @Override
    public Location getLastLocation() {
        return lastLocation;
    }

    @Override
    public void onLocationPermissionCheckFailed() {
        mainPage.makeMessage(getActivity().getResources().getString(R.string.main_page_presenter_permission_failed_message));
    }

    @Override
    public void onShowPaymentOnMapPressed(LatLng location) {
        Intent intent = new Intent(mainPage.getActivity(), MapsActivity.class);
        intent.putExtra(IMainPagePresenter.LOCATION_EXTRA_KEY, location);
        mainPage.getActivity().startActivity(intent);
    }

    @Override
    public void onManageLocationPressed() {
        mainPage.showLocationEnableDialog(dataManager.getLocationStatus());
    }
    //</editor-fold>

    //<editor-fold desc="Categories">
    private void addDefaultCategories(AccountDTO account) {
        String accountName = account.getAccountName();
        dataManager.saveCategory(new CategoryDTO(accountName, "Salary", PaymentTypeDTO.INCOME));
        dataManager.saveCategory(new CategoryDTO(accountName, "Products", PaymentTypeDTO.EXPENSE));
        dataManager.saveCategory(new CategoryDTO(accountName, "Entertainment", PaymentTypeDTO.EXPENSE));
    }

    @Override
    public void onNewCategoryTyped(String categoryName, PaymentTypeVO type) {
        onNewCategoryAdded(categoryName, type);
    }

    @Override
    public void onNewCategoryAdded(String categoryName, PaymentTypeVO type) {
        dataManager.saveCategory(new CategoryDTO(currentAccount.getAccountName(), categoryName, PaymentTypeDTO.valueOf(type.name())));
        updateCategories();
    }

    private void updateCategories() {
        dataManager.getCategories(currentAccount, PaymentTypeDTO.INCOME).subscribe(dto ->
                incomeCategoriesForAccount = dto);
        dataManager.getCategories(currentAccount, PaymentTypeDTO.EXPENSE).subscribe(dto ->
                expenseCategoriesForAccount = dto);
    }
    //</editor-fold>

    //<editor-fold desc="calculate debts">
    @Override
    public void onCalculateDebtsButtonPressed() {
        mainPage.showIfPaymentsAreDistributedEvenlyDialog();
    }

    @Override
    public void onEvenlyDistributionPressed(boolean evenly) {
        List<Obligation> result = new ArrayList<>();
        if (evenly) {
            for (CurrencyDTO currency : CurrencyDTO.values()) {
                List<PaymentsAndAssignedPayers> pp = new ArrayList<>();
                List<PaymentDTO> paymentsForCurrency = Stream.of(paymentsForAccount).filter(p -> p.getCurrency().equals(currency)).collect(Collectors.toList());
                List<String> names = Stream.of(paymentsForCurrency).map(PaymentDTO::getPayerName).distinct().collect(Collectors.toList());
                pp.add(new PaymentsAndAssignedPayers(paymentsForCurrency, names));
                List<Obligation> obligations = new MoneyResolver().calculateEvenlyDistributedPayments(pp, true);
                result.addAll(obligations);
            }
            mainPage.showDebtsResultDialog(result);
        } else {
            selectedPayments = new HashMap<>();
            processDebtsPaymentsSelection("", new ArrayList<>());
        }
    }
    //</editor-fold>

    //<editor-fold desc="Selection">
    @Override
    public void processDebtsPaymentsSelection(String name, ArrayList<PaymentVO> payments) {
        if (payersForSelection == null) {
            payersForSelection = new ArrayList<>();
            payersForSelection.addAll(payersForAccount);
        } else {
            ArrayList<PaymentVO> p = new ArrayList<>();
            try {
                for (PaymentVO payment : payments)
                    p.add((PaymentVO) payment.clone());
            } catch (CloneNotSupportedException e) {

            }
            selectedPayments.put(name, p);
            payersForSelection.remove(name);
        }
        if (payersForSelection.isEmpty()) {
            payersForSelection = null;
            mainPage.setSelectionModOff();
            MoneyResolver resolver = new MoneyResolver();
            ArrayList<String> tPayers = new ArrayList<>();
            tPayers.addAll(payersForAccount);
            List<Obligation> obligations = resolver.calculateNotEvenlyDistributedPayments(selectedPayments, tPayers, true);
            mainPage.showDebtsResultDialog(obligations);
            selectedPayments.clear();
        } else
            for (String s : payersForSelection)
                if (!selectedPayments.containsKey(s)) {
                    mainPage.setSelectionModOn(s, SelectionType.DEBTS);
                    break;
                }
    }

    @Override
    public void onCancelSelectionPressed() {
        updatePayers();
        if (selectedPayments != null)
            selectedPayments.clear();
    }
    //</editor-fold>

    //<editor-fold desc="Statistics">

    @Override
    public void onStatisticsPressed() {
        if (paymentsForAccount.isEmpty()) {
            mainPage.makeMessage(getActivity().getResources().getString(R.string.no_payments_message));
            return;
        }
        List<CurrencyVO> currencies = Stream.of(paymentsForAccount).map(p -> dtoToVO(p.getCurrency())).distinct().toList();
        if (currencies.size() == 1)
            onCurrencyVariantChosen(currencies.get(0));
        else
            mainPage.showCurrenciesVariantsDialog(currencies);

    }

    @Override
    public void onCurrencyVariantChosen(CurrencyVO currency) {
        List<PaymentDTO> chosenPayments = Stream.of(paymentsForAccount).filter(p -> p.getCurrency().name().equals(currency.name())).toList();

        List<PaymentDTO> expenses = Stream.of(chosenPayments).filter(p -> p.getType().equals(PaymentTypeDTO.EXPENSE)).toList();
        List<String> usedExpenseCategoriesNames = Stream.of(expenses).map(PaymentDTO::getCategory).distinct().toList();
        List<CategoryDTO> usedExpenseCategories = Stream
                .of(usedExpenseCategoriesNames)
                .map(s -> Stream
                        .of(expenseCategoriesForAccount)
                        .filter(c -> c.getCategoryName().equals(s))
                        .toList()
                        .get(0)
                )
                .toList();
        HashMap<String, Float> expenseRatios = new HashMap<>();
        HashMap<String, Double> expenseCategoriesValues = new HashMap<>();
        Double sum = Stream.of(expenses).mapToDouble(PaymentDTO::getAmount).sum();
        double otherSum = 0;
        for (CategoryDTO category : usedExpenseCategories) {
            Double localSum = Stream.of(expenses).
                    filter(p -> p.getCategory().equals(category.getCategoryName())).
                    mapToDouble(PaymentDTO::getAmount).
                    sum();
            if (localSum != 0) {
                float ratio = (float) (localSum / sum);
                if (ratio < MIN_RATIO_FOR_CATEGORY) {
                    otherSum += localSum;
                    continue;
                }
                expenseRatios.put(category.getCategoryName(), ratio);
                expenseCategoriesValues.put(category.getCategoryName(), localSum);
            }
        }
        if (!NumericTools.doubleEquals(otherSum, 0)) {
            float ratio = (float) (otherSum / sum);
            String name = "Other";
            expenseRatios.put(name, ratio);
            expenseCategoriesValues.put(name, otherSum);
        }

        List<PaymentDTO> incomes = Stream.of(chosenPayments).filter(p -> p.getType().equals(PaymentTypeDTO.INCOME)).toList();
        List<String> usedIncomeCategoriesNames = Stream.of(incomes).map(PaymentDTO::getCategory).distinct().toList();
        List<CategoryDTO> usedIncomeCategories = Stream
                .of(usedIncomeCategoriesNames)
                .map(s -> Stream
                        .of(incomeCategoriesForAccount)
                        .filter(c -> c.getCategoryName().equals(s))
                        .toList()
                        .get(0)
                )
                .toList();
        HashMap<String, Float> incomeRatios = new HashMap<>();
        HashMap<String, Double> incomeCategoriesValues = new HashMap<>();
        sum = Stream.of(incomes).mapToDouble(PaymentDTO::getAmount).sum();
        otherSum = 0;
        for (CategoryDTO category : usedIncomeCategories) {
            Double localSum = Stream.of(incomes).
                    filter(p -> p.getCategory().equals(category.getCategoryName())).
                    mapToDouble(PaymentDTO::getAmount).
                    sum();
            if (localSum != 0) {
                float ratio = (float) (localSum / sum);
                if (ratio < MIN_RATIO_FOR_CATEGORY) {
                    otherSum += localSum;
                    continue;
                }
                incomeRatios.put(category.getCategoryName(), ratio);
                incomeCategoriesValues.put(category.getCategoryName(), localSum);
            }
        }
        if (!NumericTools.doubleEquals(otherSum, 0)) {
            float ratio = (float) (otherSum / sum);
            String name = "Other";
            incomeRatios.put(name, ratio);
            incomeCategoriesValues.put(name, otherSum);
        }
        mainPage.showStatistics(expenseRatios, expenseCategoriesValues, incomeRatios, incomeCategoriesValues, dtoToVO(currentAccount), currency);
    }

    @Override
    public void onCurrencyVariantConvertToUAHChosen() {
        dataManager.getExchangeRates(NetManager.RateType.BID).subscribe(c -> {
            List<PaymentDTO> expenses = Stream.of(paymentsForAccount).filter(p -> p.getType() == PaymentTypeDTO.EXPENSE).toList();
            statisticsExpenseRatios = new HashMap<>();
            statisticsExpenseCategoriesValues = new HashMap<>();
            double sum = 0;
            double otherSum = 0;
            for (PaymentDTO p : expenses) {
                double amount = p.getAmount();
                if (p.getCurrency() == CurrencyDTO.USD) {
                    sum += c.get(CurrencyDTO.USD.toString()) * amount;
                } else {
                    if (p.getCurrency() == CurrencyDTO.EUR) {
                        sum += c.get(CurrencyDTO.EUR.toString()) * amount;
                    } else if (p.getCurrency() == CurrencyDTO.UAH)
                        sum += p.getAmount();
                }
            }
            for (CategoryDTO category : expenseCategoriesForAccount) {
                List<PaymentDTO> categoryPayments = new ArrayList<>();
                for (PaymentDTO p : expenses) {
                    String pmt = p.getCategory();
                    String cat = category.getCategoryName();
                    if (pmt.equals(cat))
                        categoryPayments.add(p);
                }
                double localSum = 0;
                for (PaymentDTO p : categoryPayments) {
                    double amount = p.getAmount();
                    if (p.getCurrency() == CurrencyDTO.USD) {
                        localSum += (c.get(CurrencyDTO.USD.toString()) * amount);
                    } else {
                        if (p.getCurrency() == CurrencyDTO.EUR) {
                            localSum += (c.get(CurrencyDTO.EUR.toString()) * amount);
                        } else if (p.getCurrency() == CurrencyDTO.UAH)
                            localSum += p.getAmount();
                    }
                }
                if (localSum != 0) {
                    float ratio = (float) (localSum / sum);
                    if (ratio < MIN_RATIO_FOR_CATEGORY) {
                        otherSum += localSum;
                        continue;
                    }
                    statisticsExpenseRatios.put(category.getCategoryName(), ratio);
                    statisticsExpenseCategoriesValues.put(category.getCategoryName(), localSum);
                }
            }
            if (!NumericTools.doubleEquals(otherSum, 0)) {
                float ratio = (float) (otherSum / sum);
                String name = "Other";
                statisticsExpenseRatios.put(name, ratio);
                statisticsExpenseCategoriesValues.put(name, otherSum);
            }
        });


        dataManager.getExchangeRates(NetManager.RateType.BID).subscribe(c -> {
            List<PaymentDTO> incomes = Stream.of(paymentsForAccount).filter(p -> p.getType() == PaymentTypeDTO.INCOME).toList();
            statisticsIncomeRatios = new HashMap<>();
            statisticsIncomeCategoriesValues = new HashMap<>();
            double iSum = 0;
            double iOtherSum = 0;
            for (PaymentDTO p : incomes) {
                double amount = p.getAmount();
                if (p.getCurrency() == CurrencyDTO.USD) {
                    iSum += c.get(CurrencyDTO.USD.toString()) * amount;
                } else {
                    if (p.getCurrency() == CurrencyDTO.EUR) {
                        iSum += c.get(CurrencyDTO.EUR.toString()) * amount;
                    } else if (p.getCurrency() == CurrencyDTO.UAH)
                        iSum += p.getAmount();
                }
            }
            for (CategoryDTO category : incomeCategoriesForAccount) {
                List<PaymentDTO> categoryPayments = new ArrayList<>();
                for (PaymentDTO p : incomes) {
                    String pmt = p.getCategory();
                    String cat = category.getCategoryName();
                    if (pmt.equals(cat))
                        categoryPayments.add(p);
                }
                double localSum = 0;
                for (PaymentDTO p : categoryPayments) {
                    double amount = p.getAmount();
                    if (p.getCurrency() == CurrencyDTO.USD) {
                        localSum += (c.get(CurrencyDTO.USD.toString()) * amount);
                    } else {
                        if (p.getCurrency() == CurrencyDTO.EUR) {
                            localSum += (c.get(CurrencyDTO.EUR.toString()) * amount);
                        } else if (p.getCurrency() == CurrencyDTO.UAH)
                            localSum += p.getAmount();
                    }
                }
                if (localSum != 0) {
                    float ratio = (float) (localSum / iSum);
                    if (ratio < MIN_RATIO_FOR_CATEGORY) {
                        iOtherSum += localSum;
                        continue;
                    }
                    statisticsIncomeRatios.put(category.getCategoryName(), ratio);
                    statisticsIncomeCategoriesValues.put(category.getCategoryName(), localSum);
                }
            }
            if (!NumericTools.doubleEquals(iOtherSum, 0)) {
                float ratio = (float) (iOtherSum / iSum);
                String name = "Other";
                statisticsIncomeRatios.put(name, ratio);
                statisticsIncomeCategoriesValues.put(name, iOtherSum);
            }
        });
        mainPage.showStatistics(statisticsExpenseRatios, statisticsExpenseCategoriesValues, statisticsIncomeRatios, statisticsIncomeCategoriesValues, dtoToVO(currentAccount), CurrencyVO.UAH);
    }

    //</editor-fold>

    //<editor-fold desc="dto <-> vo">
    private PaymentVO dtoToVO(PaymentDTO dto) {
        PaymentVO payment = new PaymentVO(dto.getAccountName(),
                dto.getPayerName(),
                PaymentTypeVO.valueOf(dto.getType().name()),
                dto.getDateTime(),
                dto.getLatitude(),
                dto.getLongitude(),
                CurrencyVO.valueOf(dto.getCurrency().name()),
                dto.getAmount(),
                dto.getCategory(),
                dto.getComment());
        payment.setId(dto.getId());
        return payment;
    }

    private PaymentDTO voToDTO(PaymentVO vo) {
        PaymentDTO payment = new PaymentDTO(vo.getAccount(),
                vo.getPayerName(),
                PaymentTypeDTO.valueOf(vo.getType().name()),
                vo.getDt(),
                vo.getLatitude(),
                vo.getLongitude(),
                CurrencyDTO.valueOf(vo.getCurrency().name()),
                vo.getAmount(),
                vo.getCategory(),
                vo.getComment());
        payment.setId(vo.getId());
        payment.setId(vo.getId());
        return payment;
    }

    private ArrayList<PaymentVO> paymentsDtoToVO(ArrayList<PaymentDTO> dtos) {
        if (dtos.isEmpty())
            return new ArrayList<>();
        return new ArrayList<>(Stream.of(dtos).map(dto -> dtoToVO(dto)).collect(Collectors.toList()));
    }

    private ArrayList<PaymentDTO> paymentsVoToDTO(ArrayList<PaymentVO> vos) {
        if (vos.isEmpty())
            return new ArrayList<>();
        return new ArrayList<>(Stream.of(vos).map(vo -> voToDTO(vo)).collect(Collectors.toList()));
    }

    private CategoryVO dtoToVO(CategoryDTO dto) {
        return new CategoryVO(dto.getId(), dto.getAccount(), dto.getCategoryName(), PaymentTypeVO.valueOf(dto.getType().name()));
    }

    private ArrayList<CategoryVO> categoriesDtoToVO(ArrayList<CategoryDTO> dtos) {
        if (dtos.isEmpty())
            return new ArrayList<>();
        return new ArrayList<>(Stream.of(dtos).map(dto -> dtoToVO(dto)).collect(Collectors.toList()));
    }

    private AccountVO dtoToVO(AccountDTO dto) {
        return new AccountVO(dto.getAccountName(), dtoToVO(dto.getAccountType()));
    }

    private AccountDTO voToDTO(AccountVO vo) {
        return new AccountDTO(vo.getAccountName(), voToDTO(vo.getAccountType()));
    }

    private DailyPaymentsVO dtoToVO(DailyPaymentsDTO dto) {
        DailyPaymentsVO vo = new DailyPaymentsVO();
        vo.payments = paymentsDtoToVO(dto.payments);
        return vo;
    }

    private ArrayList<AccountVO> accountsDtoToVO(ArrayList<AccountDTO> dtos) {
        if (dtos.isEmpty())
            return new ArrayList<>();
        return new ArrayList<>(Stream.of(dtos).map(dto -> dtoToVO(dto)).collect(Collectors.toList()));
    }

    private AccountTypeVO dtoToVO(AccountTypeDTO dto) {
        return AccountTypeVO.valueOf(dto.toString());
    }

    private AccountTypeDTO voToDTO(AccountTypeVO vo) {
        return AccountTypeDTO.valueOf(vo.toString());
    }

    private CurrencyVO dtoToVO(CurrencyDTO dto) {
        return CurrencyVO.valueOf(dto.name());
    }

    private CurrencyDTO dtoToVO(CurrencyVO vo) {
        return CurrencyDTO.valueOf(vo.name());
    }
    //</editor-fold>
}




