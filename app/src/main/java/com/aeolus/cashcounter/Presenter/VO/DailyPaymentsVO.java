package com.aeolus.cashcounter.Presenter.VO;

import java.util.ArrayList;

/**
 * Created by Yuriy on 08.07.2017.
 */

public class DailyPaymentsVO {
    public ArrayList<PaymentVO> payments;

    public DailyPaymentsVO() {
        payments = new ArrayList<>();
    }

}
