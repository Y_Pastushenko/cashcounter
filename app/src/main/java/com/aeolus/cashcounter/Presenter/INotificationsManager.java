package com.aeolus.cashcounter.Presenter;

import android.content.Context;

/**
 * Created by Yuriy on 01.10.2017.
 */

public interface INotificationsManager {

    void enableNotifications(Context context, long time, long interval);

    void disableNotifications(Context context);

    boolean checkIfNotificationsEnabled(Context context);
}
