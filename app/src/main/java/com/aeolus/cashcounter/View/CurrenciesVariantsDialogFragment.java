package com.aeolus.cashcounter.View;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.aeolus.cashcounter.Presenter.IMainPagePresenter;
import com.aeolus.cashcounter.Presenter.VO.CurrencyVO;
import com.aeolus.cashcounter.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kostya on 30.09.2017.
 */

public class CurrenciesVariantsDialogFragment extends DialogFragment {
    MainPageActivity activity;
    static IMainPagePresenter presenter;
    private static List<CurrencyVO> currencies;
    private static final String USD_CHECKED = "usd";
    private static final String EUR_CHECKED = "eur";
    private static final String UAH_CHECKED = "uah";
    private static final String CONVERT_CHECKED = "convert";


    @BindView(R.id.dialog_currencies_variants_usd_rb)
    RadioButton usdRB;

    @BindView(R.id.dialog_currencies_variants_eur_rb)
    RadioButton eurRB;

    @BindView(R.id.dialog_currencies_variants_uah_rb)
    RadioButton uahRB;

    @BindView(R.id.dialog_currencies_variants_convert_rb)
    RadioButton convertRB;

    @BindView(R.id.dialog_currencies_variants_ok_button)
    Button okButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_currencies_variants, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        ButterKnife.bind(this, view);
        usdRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    eurRB.setChecked(false);
                    uahRB.setChecked(false);
                    convertRB.setChecked(false);
                }
            }
        });
        eurRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    usdRB.setChecked(false);
                    uahRB.setChecked(false);
                    convertRB.setChecked(false);
                }
            }
        });
        uahRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    usdRB.setChecked(false);
                    eurRB.setChecked(false);
                    convertRB.setChecked(false);
                }
            }
        });
        convertRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    usdRB.setChecked(false);
                    uahRB.setChecked(false);
                    eurRB.setChecked(false);
                }
            }
        });
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (convertRB.isChecked())
                    presenter.onCurrencyVariantConvertToUAHChosen();
                else if (usdRB.isChecked())
                    presenter.onCurrencyVariantChosen(CurrencyVO.USD);
                else if (eurRB.isChecked())
                    presenter.onCurrencyVariantChosen(CurrencyVO.EUR);
                else if (uahRB.isChecked())
                    presenter.onCurrencyVariantChosen(CurrencyVO.UAH);
                currencies = null;
                dismiss();
            }
        });
        if (!currencies.contains(CurrencyVO.USD))
            usdRB.setVisibility(View.GONE);
        if (!currencies.contains(CurrencyVO.EUR))
            eurRB.setVisibility(View.GONE);
        if (!currencies.contains(CurrencyVO.UAH))
            uahRB.setVisibility(View.GONE);
        if (savedInstanceState == null)
            convertRB.setChecked(true);
        else {
            usdRB.setChecked(savedInstanceState.getBoolean(USD_CHECKED));
            eurRB.setChecked(savedInstanceState.getBoolean(EUR_CHECKED));
            uahRB.setChecked(savedInstanceState.getBoolean(UAH_CHECKED));
            convertRB.setChecked(savedInstanceState.getBoolean(CONVERT_CHECKED));
        }
        return view;

    }

    public void setActivity(MainPageActivity activity) {
        this.activity = activity;
        presenter = activity.presenter;
    }

    public void setCurrencies(List<CurrencyVO> currencies) {
        this.currencies = currencies;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(USD_CHECKED, usdRB.isChecked());
        outState.putBoolean(EUR_CHECKED, eurRB.isChecked());
        outState.putBoolean(UAH_CHECKED, uahRB.isChecked());
        outState.putBoolean(CONVERT_CHECKED, convertRB.isChecked());
        super.onSaveInstanceState(outState);
    }
}
