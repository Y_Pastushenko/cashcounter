package com.aeolus.cashcounter.View;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.aeolus.cashcounter.Presenter.IMainPagePresenter;
import com.aeolus.cashcounter.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yuriy on 10.05.2017.
 */

public class AccountDetailsDialogFragment extends DialogFragment {
    static IView mainPage;
    static IMainPagePresenter presenter;

    private String totalIncome;
    private static final String TOTAL_INCOME_KEY = "total_income_key";

    private String totalExpense;
    private static final String TOTAL_EXPENSE_KEY = "total_expense_key";

    private String summary;
    private static final String SUMMARY_KEY = "summary_key";

    //<editor-fold desc="Bindings">
    @BindView(R.id.dialog_account_details_account_name_tv)
    TextView accountNameTV;

    @BindView(R.id.dialog_account_details_total_income_tv)
    TextView totalIncomeTV;

    @BindView(R.id.dialog_account_details_total_expense_tv)
    TextView totalExpenseTV;

    @BindView(R.id.dialog_account_details_summary_tv)
    TextView summaryTV;

    @BindView(R.id.dialog_account_details_account_type_tv)
    TextView accountTypeTV;

    @BindView(R.id.account_details_dialog_calculate_debts_button_divider)
    View calculateButtonDivider;

    //</editor-fold>

    public static void setActivity(IView activity) {
        mainPage = activity;
        presenter = activity.getPresenter();
    }

    public void setTotalIncome(String totalIncome) {
        this.totalIncome = totalIncome;
    }

    public void setTotalExpense(String totalExpense) {
        this.totalExpense = totalExpense;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_account_details, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        ButterKnife.bind(this, view);
        if (savedInstanceState != null) {
            totalIncome = savedInstanceState.getString(TOTAL_INCOME_KEY);
            totalExpense = savedInstanceState.getString(TOTAL_EXPENSE_KEY);
            summary = savedInstanceState.getString(SUMMARY_KEY);
        }
        accountNameTV.setText(presenter.getCurrentAccount().getAccountName());
        accountTypeTV.setText(presenter.getCurrentAccount().getAccountType().toString());
        totalIncomeTV.setTextColor(ContextCompat.getColor(mainPage.getActivity(), R.color.colorIncome));
        totalIncomeTV.setText(totalIncome);
        totalExpenseTV.setTextColor(ContextCompat.getColor(mainPage.getActivity(), R.color.colorExpense));
        totalExpenseTV.setText(totalExpense);
        summaryTV.setText(summary);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(TOTAL_INCOME_KEY, totalIncome);
        outState.putString(TOTAL_EXPENSE_KEY, totalExpense);
        outState.putString(SUMMARY_KEY, summary);
        super.onSaveInstanceState(outState);
    }
}
