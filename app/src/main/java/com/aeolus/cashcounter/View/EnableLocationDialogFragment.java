package com.aeolus.cashcounter.View;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.aeolus.cashcounter.Presenter.IMainPagePresenter;
import com.aeolus.cashcounter.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Kostya on 10.10.2017.
 */

public class EnableLocationDialogFragment extends DialogFragment {

    IMainPagePresenter presenter;

    @BindView(R.id.dialog_fragment_enable_switch)
    Switch enableSwitch;
    static boolean allowed;
    private boolean enabled;

    public void setPresenter(IMainPagePresenter presenter) {
        this.presenter = presenter;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        allowed = false;
        View view = inflater.inflate(R.layout.dialog_fragment_enable_location, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        ButterKnife.bind(this, view);
        EnableLocationDialogFragment dialog = this;
        enableSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (!allowed)
                        enableSwitch.setChecked(false);
                    presenter.onEnableLocationPressed(dialog);
                }
                else
                    presenter.onDisableLocationPressed();
            }
        });
        if (enabled)
            setChecked();
        return view;
    }

    public void setChecked(){
        allowed = true;
        enableSwitch.setChecked(true);
        allowed = false;
    }
}
