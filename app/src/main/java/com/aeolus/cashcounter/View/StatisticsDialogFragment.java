package com.aeolus.cashcounter.View;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.aeolus.cashcounter.Presenter.BL.NumericTools;
import com.aeolus.cashcounter.Presenter.IMainPagePresenter;
import com.aeolus.cashcounter.Presenter.VO.AccountTypeVO;
import com.aeolus.cashcounter.Presenter.VO.AccountVO;
import com.aeolus.cashcounter.Presenter.VO.CurrencyVO;
import com.aeolus.cashcounter.R;
import com.annimon.stream.Stream;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yuriy on 28/09/2017.
 */

public class StatisticsDialogFragment extends DialogFragment implements OnChartValueSelectedListener {

    //<editor-fold desc="Bindings">
    @BindView(R.id.fragment_statistics_chart)
    PieChart chart;
    @BindView(R.id.fragment_statistics_total_tv)
    TextView totalTV;
    @BindView(R.id.fragment_statistics_account_tv)
    TextView accountTV;
    @BindView(R.id.fragment_statistics_incomes_rb)
    RadioButton incomesRB;
    @BindView(R.id.fragment_statistics_expenses_rb)
    RadioButton expensesRB;
    //</editor-fold>

    //<editor-fold desc="Fields">
    static PieDataSet dataSet;
    static IMainPagePresenter presenter;
    static HashMap<String, Float> expensesRatios;
    static HashMap<String, Float> incomesRatios;
    static HashMap<String, Double> expensesCategoriesValues;
    static HashMap<String, Double> incomesCategoriesValues;
    static AccountVO account;
    static CurrencyVO currency;

    //</editor-fold>

    //<editor-fold desc="LifeCycle">
    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_statistics, container, false);
        ButterKnife.bind(this, view);
        chart.setUsePercentValues(true);
        chart.getDescription().setEnabled(false);
        chart.setOnChartValueSelectedListener(this);
        chart.getLegend().setEnabled(false);
        incomesRB.setOnCheckedChangeListener(null);
        expensesRB.setOnCheckedChangeListener(null);
        incomesRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setChartIncome();
                }
            }
        });
        expensesRB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setChartExpense();
                }
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        accountTV.setText(account.getAccountName());
        if (account.getAccountType() == AccountTypeVO.GROUP) {
            incomesRB.setVisibility(View.GONE);
            expensesRB.setVisibility(View.GONE);
        }
        accountTV.setText(account.getAccountName());
        setChartIncome();
    }
    //</editor-fold>

    private void setIncomeRbChecked() {
        incomesRB.setChecked(true);
        expensesRB.setChecked(false);
    }

    private void setExpenseRbChecked() {
        incomesRB.setChecked(false);
        expensesRB.setChecked(true);
    }

    void setData(HashMap<String, Float> expensesRatios,
                 HashMap<String, Double> expensesCategoriesValues,
                 HashMap<String, Float> incomesRatios,
                 HashMap<String, Double> incomesCategoriesValues,
                 AccountVO account,
                 CurrencyVO currency) {
        StatisticsDialogFragment.expensesRatios = expensesRatios;
        StatisticsDialogFragment.incomesRatios = incomesRatios;
        StatisticsDialogFragment.expensesCategoriesValues = expensesCategoriesValues;
        StatisticsDialogFragment.incomesCategoriesValues = incomesCategoriesValues;
        StatisticsDialogFragment.account = account;
        StatisticsDialogFragment.currency = currency;

    }

    public void setPresenter(IMainPagePresenter presenter) {
        StatisticsDialogFragment.presenter = presenter;
    }

    int[] generateColorsSet() {
        int[] colors = //<editor-fold desc="Colors">
                {
                        ContextCompat.getColor(getActivity(), R.color.chart1),
                        ContextCompat.getColor(getActivity(), R.color.chart2),
                        ContextCompat.getColor(getActivity(), R.color.chart3),
                        ContextCompat.getColor(getActivity(), R.color.chart4),
                        ContextCompat.getColor(getActivity(), R.color.chart5),
                        ContextCompat.getColor(getActivity(), R.color.chart6),
                        ContextCompat.getColor(getActivity(), R.color.chart7),
                        ContextCompat.getColor(getActivity(), R.color.chart8),
                        ContextCompat.getColor(getActivity(), R.color.chart9),
                        ContextCompat.getColor(getActivity(), R.color.chart10),
                        ContextCompat.getColor(getActivity(), R.color.chart11),
                        ContextCompat.getColor(getActivity(), R.color.chart12),
                        ContextCompat.getColor(getActivity(), R.color.chart13),
                        ContextCompat.getColor(getActivity(), R.color.chart14),
                        ContextCompat.getColor(getActivity(), R.color.chart15),
                        ContextCompat.getColor(getActivity(), R.color.chart16),
                        ContextCompat.getColor(getActivity(), R.color.chart17),
                        ContextCompat.getColor(getActivity(), R.color.chart18),
                        ContextCompat.getColor(getActivity(), R.color.chart19),
                        ContextCompat.getColor(getActivity(), R.color.chart20)
                }
                //</editor-fold>
                ;
        ArrayList<Integer> list = new ArrayList<>();
        for (int i : colors)
            list.add(i);
        Collections.shuffle(list);
        return Stream.of(list).mapToInt(Integer::intValue).toArray();
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (e == null)
            return;
        if (incomesRB.isChecked())
            chart.setCenterText(NumericTools.round(incomesCategoriesValues.get(dataSet.getEntryForIndex(dataSet.getEntryIndex(e)).getLabel()), 2) + " " + currency.toString());
        else
            chart.setCenterText(NumericTools.round(expensesCategoriesValues.get(dataSet.getEntryForIndex(dataSet.getEntryIndex(e)).getLabel()), 2) + " " + currency.toString());
    }

    @Override
    public void onNothingSelected() {
        chart.setCenterText("");
    }

    private void setChartIncome() {
        setChart(incomesRatios, incomesCategoriesValues, account, currency);
        setIncomeRbChecked();
    }

    private void setChartExpense() {
        setChart(expensesRatios, expensesCategoriesValues, account, currency);
        setExpenseRbChecked();
    }

    private void setChart(HashMap<String, Float> ratios, HashMap<String, Double> categoriesValues, AccountVO account, CurrencyVO chartCurrency) {
        List<PieEntry> pieEntries = new ArrayList<>();
        for (Map.Entry<String, Float> e : ratios.entrySet()) {
            pieEntries.add(new PieEntry(e.getValue(), e.getKey()));
        }
        dataSet = new PieDataSet(pieEntries, account.getAccountName());
        dataSet.setColors(generateColorsSet());
        dataSet.setValueFormatter(new Formatter());
        PieData data = new PieData(dataSet);
        chart.clear();
        chart.setData(data);
        totalTV.setText("" + NumericTools.round(Stream.of(categoriesValues.values()).mapToDouble(Double::doubleValue).sum(), 2)
                + " " + chartCurrency.toString() + " in total.");
    }

    public class Formatter implements IValueFormatter {

        private DecimalFormat mFormat;

        public Formatter() {
            mFormat = new DecimalFormat("###,###,##0.0"); // use one decimal
        }

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            return mFormat.format(value) + "%";
        }
    }
}
