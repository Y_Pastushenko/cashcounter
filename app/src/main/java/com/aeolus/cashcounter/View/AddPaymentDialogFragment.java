package com.aeolus.cashcounter.View;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.aeolus.cashcounter.Presenter.IMainPagePresenter;
import com.aeolus.cashcounter.Presenter.VO.AccountTypeVO;
import com.aeolus.cashcounter.Presenter.VO.CurrencyVO;
import com.aeolus.cashcounter.R;
import com.aeolus.cashcounter.Presenter.VO.CategoryVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentTypeVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentVO;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yuriy on 10.05.2017.
 */

public class AddPaymentDialogFragment extends DialogFragment implements View.OnClickListener {

    //<editor-fold desc="Fields">
    private static IMainPagePresenter presenter;

    private static PaymentTypeVO type;
    private static final String TYPE_KEY = "type_key";

    private static ArrayList<String> categoriesNames;
    private static final String CATEGORIES_NAMES_KEY = "categories_names_key";

    private static ArrayList<String> payers;
    private static final String PAYERS_KEY = "payers_key";

    private static String accountName;
    private static final String ACCOUNT_NAME_KEY = "account_name";

    private static LatLng chosenLocation = null;
    private static final String CHOSEN_LOCATION_KEY = "chosen_location";

    private static boolean currentLocationMod;
    private static final String CURRENT_LOCATION_MOD = "current_location_mod";

    public final static int PLACE_PICKER_REQUEST = 1;
    //</editor-fold>

    //<editor-fold desc="Bindings">

    @BindView(R.id.add_payment_dialog_amount_et)
    EditText amountET;
    private static final String AMOUNT_KEY = "amount_key";

    @BindView(R.id.add_payment_dialog_category_actv)
    AutoCompleteTextView categoryACTV;
    private static final String CATEGORY_KEY = "category_key";

    @BindView(R.id.add_payment_dialog_comment_et)
    EditText commentET;
    private static final String COMMENT_KEY = "comment_key";

    @BindView(R.id.add_payment_dialog_ok_button)
    Button okButton;

    @BindView(R.id.add_payment_dialog_added_just_now_cb)
    CheckBox addedJustNowCheckBox;
    private static final String DATE_TIME_CHECKBOX_KEY = "date_time_checkbox_key";

    @BindView(R.id.add_payment_dialog_date_picker)
    DatePicker datePicker;

    @BindView(R.id.add_payment_dialog_time_picker)
    TimePicker timePicker;
    private static final String DATE_TIME_PICKER_KEY = "date_time_picker_key";

    @BindView(R.id.add_payment_dialog_date_radio_button)
    RadioButton dateRadioButton;
    private static final String DATE_RB_KEY = "date_rb_key";

    @BindView(R.id.add_payment_dialog_time_radio_button)
    RadioButton timeRadioButton;

    @BindView(R.id.add_payment_dialog_me_check_box)
    CheckBox meCheckBox;
    private static final String ME_CHECK_BOX_KEY = "me_check_box_key";

    @BindView(R.id.add_payment_dialog_payer_name_actv)
    AutoCompleteTextView payerNameACTV;
    private static final String PAYER_NAME_KEY = "payer_name_key";

    @BindView(R.id.add_payment_dialog_currency_spinner)
    Spinner currencySpinner;
    private static final String CURRENCY_SPINNER_KEY = "currency_spinner_key";

    @BindView(R.id.add_payment_dialog_location_off_rb)
    RadioButton locationOffRb;
    private static final String LOCATION_OFF_RB_KEY = "location_off_rb_key";

    @BindView(R.id.add_payment_dialog_current_location_rb)
    RadioButton currentLocationRB;

    @BindView(R.id.add_payment_dialog_choose_location_button)
    Button chooseLocationButton;
    //</editor-fold>

    public void setData(IMainPagePresenter presenter, String accountName, PaymentTypeVO type, ArrayList<CategoryVO> categoriesList, ArrayList<String> payers, boolean currentLocationMod) {
        AddPaymentDialogFragment.presenter = presenter;
        AddPaymentDialogFragment.accountName = accountName;
        AddPaymentDialogFragment.type = type;
        AddPaymentDialogFragment.categoriesNames = new ArrayList<>(Stream.of(categoriesList).map(CategoryVO::getCategoryName).collect(Collectors.toList()));
        AddPaymentDialogFragment.payers = payers;
        AddPaymentDialogFragment.currentLocationMod = currentLocationMod;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = inflater.inflate(R.layout.dialog_fragment_add_payment, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        ButterKnife.bind(this, view);
        datePicker.updateDate(DateTime.now().getYear(),
                DateTime.now().getMonthOfYear() - 1,
                DateTime.now().getDayOfMonth());
        timePicker.setIs24HourView(true);
        addedJustNowCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    datePicker.setVisibility(View.GONE);
                    timePicker.setVisibility(View.GONE);
                    dateRadioButton.setVisibility(View.GONE);
                    dateRadioButton.setChecked(false);
                    timeRadioButton.setVisibility(View.GONE);
                    timeRadioButton.setChecked(false);
                } else {
                    dateRadioButton.setVisibility(View.VISIBLE);
                    dateRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                timeRadioButton.setChecked(false);
                                timePicker.setVisibility(View.GONE);
                                datePicker.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                    timeRadioButton.setVisibility(View.VISIBLE);
                    timeRadioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                dateRadioButton.setChecked(false);
                                datePicker.setVisibility(View.GONE);
                                timePicker.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                    dateRadioButton.setChecked(true);
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        timePicker.setHour(DateTime.now().getHourOfDay());
                        timePicker.setMinute(DateTime.now().getMinuteOfHour());
                    } else {
                        timePicker.setCurrentHour(DateTime.now().getHourOfDay());
                        timePicker.setCurrentMinute(DateTime.now().getMinuteOfHour());
                    }
                }
            }
        });
        addedJustNowCheckBox.setChecked(true);
        categoryACTV.setAdapter(new ArrayAdapter<>(presenter.getActivity(), android.R.layout.simple_spinner_item, categoriesNames));
        payerNameACTV.setAdapter(new ArrayAdapter<>(presenter.getActivity(), android.R.layout.simple_spinner_item, payers));
        meCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    payerNameACTV.setVisibility(View.GONE);
                else
                    payerNameACTV.setVisibility(View.VISIBLE);
            }
        });
        meCheckBox.setChecked(true);
        List<String> currency = Stream.of(Arrays.asList(CurrencyVO.values())).map(CurrencyVO::toString).collect(Collectors.toList());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(presenter.getActivity(), android.R.layout.simple_spinner_item, currency);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currencySpinner.setAdapter(adapter);
        currencySpinner.setSelection(currency.indexOf(CurrencyVO.UAH.toString()));

        ColorStateList buttonNormColor = chooseLocationButton.getTextColors();
        if (!currentLocationMod)
            currentLocationRB.setVisibility(View.GONE);
        currentLocationRB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (presenter.getLastLocation() == null) {
                    Toast.makeText(presenter.getActivity(), R.string.add_payment_dialog_location_not_ready_message, Toast.LENGTH_SHORT).show();
                    currentLocationRB.setChecked(false);
                    return;
                }
                currentLocationRB.setChecked(true);
                locationOffRb.setChecked(false);
                chooseLocationButton.setTextColor(buttonNormColor);
            }
        });
        locationOffRb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentLocationRB.setChecked(false);
                locationOffRb.setChecked(true);
                chooseLocationButton.setTextColor(buttonNormColor);
            }
        });
        chooseLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentLocationRB.setChecked(false);
             //   presenter.chooseLocationOnMapPressed();
                chooseOnMap();
            }
        });
        locationOffRb.setChecked(true);
        okButton.setOnClickListener(this);
        if (presenter.getCurrentAccount().getAccountType() == AccountTypeVO.PERSONAL) {
            meCheckBox.setVisibility(View.GONE);
            payerNameACTV.setVisibility(View.GONE);
        }
        if (bundle != null) {
            currencySpinner.setSelection(currency.indexOf(bundle.getString(CURRENCY_SPINNER_KEY)));
            amountET.setText(bundle.getString(AMOUNT_KEY));
            categoryACTV.setText(bundle.getString(CATEGORY_KEY));
            commentET.setText(bundle.getString(COMMENT_KEY));
            if (!bundle.getBoolean(DATE_TIME_CHECKBOX_KEY))
                addedJustNowCheckBox.setChecked(false);
            DateTime dateTime = (DateTime) bundle.getSerializable(DATE_TIME_PICKER_KEY);
            datePicker.updateDate(dateTime.getYear(), dateTime.getMonthOfYear(), dateTime.getDayOfMonth());
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                timePicker.setHour(dateTime.getHourOfDay());
            else
                timePicker.setCurrentHour(dateTime.getHourOfDay());
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                timePicker.setMinute(dateTime.getMinuteOfHour());
            else
                timePicker.setCurrentMinute(dateTime.getMinuteOfHour());
            if (!bundle.getBoolean(DATE_RB_KEY))
                timeRadioButton.setChecked(true);
            if (!bundle.getBoolean(ME_CHECK_BOX_KEY)) {
                meCheckBox.setChecked(false);
                payerNameACTV.setText(bundle.getString(PAYER_NAME_KEY));
            }
            if (bundle.getParcelable(CHOSEN_LOCATION_KEY) != null) {
                currentLocationRB.setChecked(false);
                chosenLocation = bundle.getParcelable(CHOSEN_LOCATION_KEY);
                chooseLocationButton.setTextColor(ContextCompat.getColor(presenter.getActivity(), R.color.colorAccent));
                currentLocationRB.setChecked(false);
                locationOffRb.setChecked(false);
            } else {
                if (bundle.getBoolean(LOCATION_OFF_RB_KEY))
                    locationOffRb.setChecked(true);
                else
                    currentLocationRB.setChecked(true);
            }

        }
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_payment_dialog_ok_button:
                if (amountET.getText().length() == 0 ||
                        categoryACTV.getText().length() == 0 ||
                        commentET.getText().length() == 0 || (!meCheckBox.isChecked() && payerNameACTV.length() == 0)) {
                    Toast.makeText(presenter.getActivity(), R.string.add_payment_dialog_empty_fields_message, Toast.LENGTH_SHORT).show();
                    break;
                }
                String categoryText = categoryACTV.getText().toString();
                if (!categoriesNames.contains(categoryText))
                    presenter.onNewCategoryTyped(categoryText, type);
                if (!meCheckBox.isChecked())
                    if (!payers.contains(payerNameACTV.getText().toString()))
                        payers.add(payerNameACTV.getText().toString());
                double latitude;
                double longitude;
                if (currentLocationRB.isChecked()) {
                    latitude = presenter.getLastLocation().getLatitude();
                    longitude = presenter.getLastLocation().getLongitude();
                } else {
                    if (!locationOffRb.isChecked()) {
                        latitude = chosenLocation.latitude;
                        longitude = chosenLocation.longitude;
                    } else {
                        latitude = -1.0;
                        longitude = -1.0;
                    }
                }
                presenter.onNewPaymentAdded(
                        new PaymentVO(accountName,
                                meCheckBox.isChecked() ? "" : payerNameACTV.getText().toString(),
                                type,
                                (addedJustNowCheckBox.isChecked()) ?
                                        DateTime.now() :
                                        new DateTime(datePicker.getYear(),
                                                datePicker.getMonth() + 1,
                                                datePicker.getDayOfMonth(),
                                                (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ?
                                                        timePicker.getHour() :
                                                        timePicker.getCurrentHour(),
                                                (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ?
                                                        timePicker.getMinute() :
                                                        timePicker.getCurrentMinute()),
                                latitude,
                                longitude,
                                CurrencyVO.valueOf(currencySpinner.getSelectedItem().toString()),
                                Double.parseDouble(amountET.getText().toString()),
                                categoryACTV.getText().toString(),
                                commentET.getText().toString()));
                //dismiss();
                clearAll();
                break;
            default:
                break;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        presenter.onAllPaymentsAdded();
    }

    void clearAll() {
        amountET.setText("");
        categoryACTV.setText("");
        commentET.setText("");
        addedJustNowCheckBox.setChecked(true);
        chosenLocation = null;
        chooseLocationButton.setTextColor(getResources().getColor(R.color.place_autocomplete_prediction_primary_text));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(TYPE_KEY, type);
        outState.putStringArrayList(CATEGORIES_NAMES_KEY, categoriesNames);
        outState.putStringArrayList(PAYERS_KEY, payers);
        outState.putString(ACCOUNT_NAME_KEY, accountName);
        outState.putParcelable(CHOSEN_LOCATION_KEY, chosenLocation);
        outState.putBoolean(CURRENT_LOCATION_MOD, currentLocationMod);
        outState.putString(AMOUNT_KEY, amountET.getText().toString());
        outState.putString(CATEGORY_KEY, categoryACTV.getText().toString());
        outState.putString(COMMENT_KEY, commentET.getText().toString());
        outState.putBoolean(DATE_TIME_CHECKBOX_KEY, addedJustNowCheckBox.isChecked());
        outState.putSerializable(DATE_TIME_PICKER_KEY, new DateTime(datePicker.getYear(),
                datePicker.getMonth() + 1,
                datePicker.getDayOfMonth(),
                (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ?
                        timePicker.getHour() :
                        timePicker.getCurrentHour(),
                (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ?
                        timePicker.getMinute() :
                        timePicker.getCurrentMinute()));
        outState.putBoolean(DATE_RB_KEY, dateRadioButton.isChecked());
        outState.putBoolean(ME_CHECK_BOX_KEY, meCheckBox.isChecked());
        outState.putString(PAYER_NAME_KEY, payerNameACTV.getText().toString());
        outState.putString(CURRENCY_SPINNER_KEY, currencySpinner.getSelectedItem().toString());
        outState.putBoolean(LOCATION_OFF_RB_KEY, locationOffRb.isChecked());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case PLACE_PICKER_REQUEST:
                    setChosenLocation(PlacePicker.getPlace(getActivity(), data).getLatLng());
                    break;
                default:
                    break;
            }
        }
    }

    void setChosenLocation(LatLng chosenLocation) {
        if (chosenLocation != null) {
            AddPaymentDialogFragment.chosenLocation = chosenLocation;
            chooseLocationButton.setTextColor(ContextCompat.getColor(presenter.getActivity(), R.color.colorAccent));
            currentLocationRB.setChecked(false);
            locationOffRb.setChecked(false);
        }
    }

    public void chooseOnMap() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException e) {
            Toast.makeText(getActivity(), "Google Play Services Error", Toast.LENGTH_SHORT).show();
        }
    }
}
