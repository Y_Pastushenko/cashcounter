package com.aeolus.cashcounter.View;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.aeolus.cashcounter.Presenter.IMainPagePresenter;

/**
 * Created by Kostya on 10.10.2017.
 */

public class EnableGpsInSettingsDialogFragment extends DialogFragment {

    IMainPagePresenter presenter;

    public void setPresenter(IMainPagePresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        presenter.onEnableGpsInSettingsPressed();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        presenter.onDisableLocationPressed();
                        dialog.dismiss();
                    }
                });
        return builder.create();
    }
}
