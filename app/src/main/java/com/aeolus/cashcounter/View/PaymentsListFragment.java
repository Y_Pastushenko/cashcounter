package com.aeolus.cashcounter.View;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aeolus.cashcounter.Presenter.BL.NumericTools;
import com.aeolus.cashcounter.Presenter.IMainPagePresenter;
import com.aeolus.cashcounter.Presenter.VO.CurrencyVO;
import com.aeolus.cashcounter.Presenter.VO.DailyPaymentsVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentTypeVO;
import com.aeolus.cashcounter.Presenter.VO.SelectionType;
import com.aeolus.cashcounter.R;
import com.aeolus.cashcounter.Presenter.VO.PaymentVO;
import com.annimon.stream.Stream;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yuriy on 06.05.2017.
 */

public class PaymentsListFragment extends Fragment {

    //<editor-fold desc="Fields">
    private IMainPagePresenter presenter;
    private MainPageActivity activity;
    private ArrayList<DailyPaymentsVO> paymentsList;
    private PaymentAdapter adapter;
    private ArrayList<PaymentVO> selectedPayments = new ArrayList<>();
    private String selectionName = "";
    private static final String SELECTION_NAME_KEY = "selection_name";
    private boolean selectionModOn;
    private static final String SELECTION_MOD_ON_KEY = "selection_mod_on_key";
    SelectionType selectionType;
    private static final String SELECTION_TYPE_KEY = "selection_type_key";
    boolean groupCosts;
    boolean itemSelected = false;
    LinearLayoutManager layoutManager;
    private static final String SELECTIONS_KEY = "selection_key";
    //</editor-fold>

    //<editor-fold desc="Bindings">
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.no_payments_message_layout)
    RelativeLayout noPaymentsMessageLayout;

    @BindView(R.id.fab_menu)
    FloatingActionMenu fabMenu;

    @BindView(R.id.fragment_payments_list_select_layout)
    LinearLayout selectLayout;

    @BindView(R.id.fragment_payments_list_select_all_cb)
    CheckBox selectAllCB;

    @BindView(R.id.fragment_payments_list_ok_button)
    Button okButton;

    @BindView(R.id.fragment_payments_list_cancel_button)
    Button cancelButton;

    @BindView(R.id.fragment_payments_list_selection_message_tv)
    TextView selectionMessageTV;

    @BindView(R.id.fab_income)
    FloatingActionButton incomeButton;

    @BindView(R.id.fab_expense)
    FloatingActionButton expenseButton;

    //</editor-fold>

    private class PaymentHolder extends RecyclerView.ViewHolder {
        final LinearLayout background;
        final TextView dateTV;
        final TextView totalIncomeTV;
        final TextView totalExpenseTV;
        final TextView totalTV;
        final LinearLayout paymentsContainer;

        public PaymentHolder(View itemView) {
            super(itemView);
            background = (LinearLayout) itemView.findViewById(R.id.payments_list_item_background);
            dateTV = (TextView) itemView.findViewById(R.id.payments_list_item_date_tv);
            totalIncomeTV = (TextView) itemView.findViewById(R.id.payments_list_item_total_income);
            totalExpenseTV = (TextView) itemView.findViewById(R.id.payments_list_item_total_expense);
            totalTV = (TextView) itemView.findViewById(R.id.payments_list_item_total);
            paymentsContainer = (LinearLayout) itemView.findViewById(R.id.payments_list_item_payments_container);

        }

        void setGroupCostMod(boolean set) {
            if (set) {
                totalIncomeTV.setVisibility(View.GONE);
                totalExpenseTV.setVisibility(View.GONE);
            } else {
                totalIncomeTV.setVisibility(View.VISIBLE);
                totalExpenseTV.setVisibility(View.VISIBLE);
            }
        }

        void clearPaymentsContainer() {
            paymentsContainer.removeAllViews();
        }

    }

    private class PaymentAdapter extends RecyclerView.Adapter<PaymentHolder> {
        final List<DailyPaymentsVO> paymentsForCards;
        private boolean selectAll = false;

        public PaymentAdapter(List<DailyPaymentsVO> paymentVOs) {
            this.paymentsForCards = paymentVOs;
        }

        @Override
        public PaymentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater
                    .inflate(R.layout.payments_list_item, parent, false);
            return new PaymentHolder(view);
        }

        @Override
        public void onBindViewHolder(PaymentHolder holder, int position) {
            holder.clearPaymentsContainer();
            holder.setGroupCostMod(groupCosts);
            DailyPaymentsVO payments = paymentsForCards.get(position);
            String totalIncome = "";
            String totalExpense = "";
            String summary = "";
            ArrayList<PaymentsListElement> elements = new ArrayList<>();
            for (PaymentVO p : payments.payments) {
                PaymentsListElement payment = new PaymentsListElement(getContext(), p, !groupCosts);
                long[] selectedIds = Stream.of(selectedPayments).mapToLong(PaymentVO::getId).toArray();
                if (selectionModOn && selectedPayments.size() != 0 && !itemSelected)
                    if (NumericTools.contains(selectedIds, p.getId()))
                        payment.setSelected(true);
                payment.setOnClickListener((View v) -> {
                    if (!selectionModOn) {
                        PaymentDetailsDialogFragment dialog = new PaymentDetailsDialogFragment();
                        dialog.setData(p, presenter);
                        dialog.show(getActivity().getFragmentManager(),"dialog_payment_details");
                    } else {
                        if (selectedPayments.contains(p)) {
                            selectedPayments.remove(p);
                            if (selectedPayments.isEmpty())
                                itemSelected = false;
                            payment.setSelected(false);
                        } else {
                            itemSelected = true;
                            selectedPayments.add(p);
                            payment.setSelected(true);
                        }
                    }
                });
                payment.setOnLongClickListener((View v) -> {
                    onLongPressOnPayment(p, position);
                    return true;
                });
                if (selectAll && !selectedPayments.contains(p)) {
                    payment.setSelected(true);
                    selectedPayments.add(p);
                }
                elements.add(payment);
                holder.paymentsContainer.addView(payment);
            }
            for (CurrencyVO currency : CurrencyVO.values()) {
                double currentCurrencyIncome = 0;
                double currentCurrencyExpense = 0;
                for (PaymentVO p : payments.payments)
                    if (p.getCurrency().equals(currency))
                        if (p.getType() == PaymentTypeVO.INCOME)
                            currentCurrencyIncome += p.getAmount();
                        else
                            currentCurrencyExpense += p.getAmount();
                if (currentCurrencyIncome == 0 && currentCurrencyExpense == 0)
                    continue;
                if (currentCurrencyIncome != 0)
                    totalIncome += "+" + currentCurrencyIncome + " " + currency + "\n";
                if (currentCurrencyExpense != 0)
                    totalExpense += "-" + currentCurrencyExpense + " " + currency + "\n";
                summary += "" + (currentCurrencyIncome - currentCurrencyExpense) + " " + currency + "\n";
            }
            if (totalIncome.length() >= 2)
                totalIncome = totalIncome.substring(0, totalIncome.length() - 1);
            if (totalExpense.length() >= 2)
                totalExpense = totalExpense.substring(0, totalExpense.length() - 1);
            if (summary.length() >= 2)
                summary = summary.substring(0, summary.length() - 1);
            if (totalIncome.equals(""))
                totalIncome += "0";
            if (totalExpense.equals(""))
                totalExpense += "0";
            if (summary.equals(""))
                summary += "0";
            holder.dateTV.setText(payments.payments.get(0).getDt().toLocalDate().toString());
            holder.totalIncomeTV.setText(totalIncome);
            holder.totalExpenseTV.setText(totalExpense);
            holder.totalTV.setText(summary);
            holder.dateTV.setOnClickListener((View v) -> {
                for (PaymentsListElement element : elements) {
                    if (element.getVisibility() == View.VISIBLE)
                        element.setVisibility(View.GONE);
                    element.setVisibility(View.VISIBLE);
                }
            });
        }

        @Override
        public int getItemCount() {
            return paymentsForCards.size();
        }

        void selectAll(boolean select) {
            selectAll = select;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payments_list, container, false);
        ButterKnife.bind(this, view);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectionModOn) {
                    if (selectionType == SelectionType.DEBTS)
                        presenter.processDebtsPaymentsSelection(selectionName, selectedPayments);
                    else if (selectionType == SelectionType.FOR_EXPORT) {
                        presenter.onPaymentsForExportingSelected(selectedPayments);
                        setSelectionModOff();
                    }
                    selectAll(false);
                }
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onCancelSelectionPressed();
                setSelectionModOff();
            }
        });
        selectAllCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                selectAll(isChecked);
            }
        });
        selectionMessageTV.setVisibility(View.GONE);
        selectLayout.setVisibility(View.GONE);
        fabMenu.setClosedOnTouchOutside(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        incomeButton.setLabelVisibility(View.VISIBLE);
        incomeButton.setLabelText(getResources().getString(R.string.fragment_payments_list_income_fab_label));
        expenseButton.setLabelVisibility(View.VISIBLE);
        expenseButton.setLabelText(getResources().getString(R.string.fragment_payments_list_expense_fab_label));
        incomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onAddNewIncomeButtonPressed();
                fabMenu.close(true);
            }
        });
        expenseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onAddNewExpenseButtonPressed();
                fabMenu.close(true);
            }
        });
        fabMenu.close(false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpList();
        activity.onFragmentViewCreated();
        if (savedInstanceState != null) {
            selectionModOn = savedInstanceState.getBoolean(SELECTION_MOD_ON_KEY);
            if (selectionModOn) {
                selectionType = (SelectionType) savedInstanceState.getSerializable(SELECTION_TYPE_KEY);
                selectionName = savedInstanceState.getString(SELECTION_NAME_KEY);
                setSelectionModOn(selectionName, selectionType);
                selectedPayments = savedInstanceState.getParcelableArrayList(SELECTIONS_KEY);
            }
        }
    }

    public void pinNoPaymentsMessage(boolean show) {
        if (show) {
            recyclerView.setVisibility(View.GONE);
            noPaymentsMessageLayout.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            noPaymentsMessageLayout.setVisibility(View.GONE);
        }
    }

    void setSelectionModOn(String message, SelectionType selectionType) {
        this.selectionType = selectionType;
        selectionName = message;
        selectionMessageTV.setText(message);
        selectionModOn = true;
        selectionMessageTV.setVisibility(View.VISIBLE);
        selectLayout.setVisibility(View.VISIBLE);
        adapter.notifyDataSetChanged();
    }

    void setSelectionModOff() {
        selectionType = null;
        selectionName = "";
        selectionModOn = false;
        selectedPayments.clear();
        selectionMessageTV.setVisibility(View.GONE);
        selectLayout.setVisibility(View.GONE);
        selectAllCB.setChecked(false);
        adapter.notifyDataSetChanged();
        activity.getSupportActionBar().show();
    }

    void selectAll(boolean select) {
        if (!select) {
            selectedPayments.clear();
        }
        adapter.selectAll(select);
        adapter.notifyDataSetChanged();
    }

    public void setActivity(MainPageActivity activity) {
        this.activity = activity;
    }

    void setPresenter(IMainPagePresenter presenter) {
        this.presenter = presenter;
    }

    private void setUpList() {
        paymentsList = new ArrayList<>();
        adapter = new PaymentAdapter(paymentsList);
        recyclerView.setAdapter(adapter);
    }

    public void clearList() {
        paymentsList.clear();
        adapter.notifyDataSetChanged();
    }

    public void appendToList(DailyPaymentsVO payments) {
        paymentsList.add(payments);
        adapter.notifyItemInserted(paymentsList.size() - 1);
    }

    private void onLongPressOnPayment(PaymentVO p, int position) {
        activity.showDeletePaymentDialog(p, paymentsList.get(position));
    }

    public void deletePaymentFromList(PaymentVO paymentVO, DailyPaymentsVO dailyPaymentsVO) {
        int position = paymentsList.indexOf(dailyPaymentsVO);
        paymentsList.get(position).payments.remove(paymentVO);
        if (paymentsList.get(position).payments.isEmpty())
            paymentsList.remove(position);
    }

    int getListCount() {
        return paymentsList.size();
    }

    void scrollTo(DailyPaymentsVO dailyPaymentsVO) {
        recyclerView.scrollToPosition(paymentsList.indexOf(dailyPaymentsVO));
    }

    void scrollTo(int position) {
        recyclerView.scrollToPosition(position);
    }

    int getCurrentPosition() {
        return layoutManager.findFirstCompletelyVisibleItemPosition();
    }

    public void setGroupCostsModOn() {
        groupCosts = true;
        adapter.notifyDataSetChanged();
        fabMenu.removeAllMenuButtons();
        fabMenu.addMenuButton(expenseButton);
    }

    public void setPurseModOn() {
        groupCosts = false;
        adapter.notifyDataSetChanged();
        fabMenu.removeAllMenuButtons();
        fabMenu.addMenuButton(incomeButton);
        fabMenu.addMenuButton(expenseButton);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (selectionModOn)
            outState.putParcelableArrayList(SELECTIONS_KEY, selectedPayments);
        if (selectionType != null)
            outState.putSerializable(SELECTION_TYPE_KEY, selectionType);
        outState.putBoolean(SELECTION_MOD_ON_KEY, selectionModOn);
        outState.putString(SELECTION_NAME_KEY, selectionName);
        super.onSaveInstanceState(outState);
    }
}