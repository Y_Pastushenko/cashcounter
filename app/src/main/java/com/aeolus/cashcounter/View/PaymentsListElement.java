package com.aeolus.cashcounter.View;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aeolus.cashcounter.Presenter.VO.AccountTypeVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentTypeVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentVO;
import com.aeolus.cashcounter.R;

/**
 * Created by Yuriy on 23.05.2017.
 */

public class PaymentsListElement extends LinearLayout {
    private View rootView;
    private TextView payerNameTV;
    private TextView amountTV;
    private TextView currencyTV;
    private TextView categoryTV;
    private PaymentVO payment;
    boolean purseMod;

    public PaymentVO getPayment() {
        return payment;
    }

    public PaymentsListElement(Context context) {
        super(context);
        init(context);
    }

    public PaymentsListElement(Context context, PaymentVO payment, boolean purseMod) {
        super(context);
        this.payment = payment;
        this.purseMod = purseMod;
        init(context, payment);
    }

    public PaymentsListElement(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        rootView = inflate(context, R.layout.payment_element, this);
        payerNameTV = (TextView) rootView.findViewById(R.id.payment_element_name_tv);
        amountTV = (TextView) rootView.findViewById(R.id.payment_element_amount_tv);
        currencyTV = (TextView) rootView.findViewById(R.id.payment_element_currency_tv);
        categoryTV = (TextView) rootView.findViewById(R.id.payment_element_category_tv);

        setSelected(true);
    }

    private void init(Context context, PaymentVO payment) {
        rootView = inflate(context, R.layout.payment_element, this);

        payerNameTV = (TextView) rootView.findViewById(R.id.payment_element_name_tv);
        if (purseMod)
            payerNameTV.setVisibility(GONE);
        amountTV = (TextView) rootView.findViewById(R.id.payment_element_amount_tv);
        currencyTV = (TextView) rootView.findViewById(R.id.payment_element_currency_tv);
        categoryTV = (TextView) rootView.findViewById(R.id.payment_element_category_tv);

        payerNameTV.setText(payment.getPayerName().length()==0?"You":payment.getPayerName());
        String str;
        if (payment.getType().equals(PaymentTypeVO.INCOME)) {
            str = "+" + String.valueOf(payment.getAmount());
            amountTV.setText(str);
            amountTV.setTextColor(ContextCompat.getColor(context, R.color.colorIncome));
            categoryTV.setTextColor(ContextCompat.getColor(context, R.color.colorIncome));
        } else {
            str = "-" + String.valueOf(payment.getAmount());
            amountTV.setText(str);
            amountTV.setTextColor(ContextCompat.getColor(context, R.color.colorExpense));
            categoryTV.setTextColor(ContextCompat.getColor(context, R.color.colorExpense));
        }
        currencyTV.setText(payment.getCurrency().toString());
        categoryTV.setText(payment.getCategory());
    }

    public void setSelected(boolean selected){
        if (selected) {
            rootView.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.selected_element_bg, null));
        } else {
            rootView.setBackground(null);
        }
    }

    public long getPaymentId() {
        return payment.getId();
    }
}
