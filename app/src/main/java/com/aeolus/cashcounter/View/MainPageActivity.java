package com.aeolus.cashcounter.View;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.aeolus.cashcounter.Presenter.IMainPagePresenter;
import com.aeolus.cashcounter.Presenter.MainPagePresenter;
import com.aeolus.cashcounter.Presenter.BL.Obligation;
import com.aeolus.cashcounter.Presenter.VO.AccountTypeVO;
import com.aeolus.cashcounter.Presenter.VO.CurrencyVO;
import com.aeolus.cashcounter.Presenter.VO.DailyPaymentsVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentVO;
import com.aeolus.cashcounter.Presenter.VO.SelectionType;
import com.aeolus.cashcounter.R;
import com.aeolus.cashcounter.Presenter.VO.AccountVO;
import com.aeolus.cashcounter.Presenter.VO.CategoryVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentTypeVO;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;

import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainPageActivity extends AppCompatActivity implements IView {

    //<editor-fold desc="Bindings">
    @BindView(R.id.activity_main_page_toolbar)
    Toolbar toolbar;
    @BindView(R.id.activity_main_page_spinner)
    Spinner spinner;
    @BindView(R.id.activity_main_page_account_information_iv)
    ImageView accountInformation;
    //</editor-fold>

    //<editor-fold desc="Fields">
    private static final String CURRENT_POSITION = "current_position";
    static IMainPagePresenter presenter;
    private PaymentsListFragment listFragment;
    //</editor-fold>

    @Override
    public AppCompatActivity getActivity() {
        return this;
    }

    @Override
    public IMainPagePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void makeMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showSettingsButtonPopUpMenu() {
        PopupMenu popupMenu = new PopupMenu(this, accountInformation);
        Menu menu = popupMenu.getMenu();
        popupMenu.inflate(R.menu.pop_up_menu);
        if (presenter.getCurrentAccount().getAccountType() == AccountTypeVO.PERSONAL)
            menu.findItem(R.id.settings_button_menu_calculate_debts_item).setVisible(false);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.settings_button_menu_account_summary_item:
                        presenter.onShowAccountInformationPressed();
                        return true;
                    case R.id.settings_button_menu_delete_account_item:
                        showDeleteAccountDialog();
                        return true;
                    case R.id.settings_button_menu_statistics_item:
                        presenter.onStatisticsPressed();
                        return true;
                    case R.id.settings_button_menu_calculate_debts_item:
                        presenter.onCalculateDebtsButtonPressed();
                        return true;
                    case R.id.settings_button_menu_about_item:
                        presenter.onAboutPressed();
                        return true;
                    case R.id.settings_button_menu_notifications_item:
                        presenter.onManageNotificationsPressed();
                        return true;
                    case R.id.settings_button_menu_import_item:
                        showImportPaymentsDialog();
                        return true;
                    case R.id.settings_button_menu_export_item:
                        presenter.onExportPressed();
                        return true;
                    case R.id.settings_button_menu_location_item:
                        presenter.onManageLocationPressed();
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }

    void showImportPaymentsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.import_payments_dialog_message)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                presenter.onImportPressed();
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void showAbout() {
        AboutDialogFragment dialog = new AboutDialogFragment();
        dialog.show(getFragmentManager(), "about_dialog_fragment");
    }

    @Override
    public void showNotificationsSettingsDialog(Boolean enabled, LocalTime time) {
        NotificationSettingsDialogFragment dialog = new NotificationSettingsDialogFragment();
        NotificationSettingsDialogFragment.setPresenter(presenter);
        dialog.setTime(time);
        dialog.setEnabled(enabled);
        dialog.show(getFragmentManager(), "notification_settings_dialog_fragment");
    }

    //<editor-fold desc="Lifecycle">

    @Override
    protected void onResume() {
        presenter.onActivityResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        presenter.onActivityPause();
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        FragmentManager fm = getSupportFragmentManager();
        listFragment = (PaymentsListFragment) fm.findFragmentById(R.id.main_page_fragment_container);
        if (listFragment == null) {
            listFragment = new PaymentsListFragment();
            fm.beginTransaction()
                    .add(R.id.main_page_fragment_container, listFragment)
                    .commit();
        }
        listFragment.setActivity(this);
        accountInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSettingsButtonPopUpMenu();
            }
        });
    }

    void onFragmentViewCreated() {
        MainPagePresenter mainPagePresenter = MainPagePresenter.getInstance();
        MainPagePresenter.setView(this);
        mainPagePresenter.init();
        presenter = mainPagePresenter;
        listFragment.setPresenter(presenter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(CURRENT_POSITION, listFragment.getCurrentPosition());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        int position = savedInstanceState.getInt(CURRENT_POSITION);
        if (position != -1)
            listFragment.scrollTo(position);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case IMainPagePresenter.FILE_CHOSEN_REQUEST:
                    presenter.onFileSelected(data.getData());
                    break;
                default:
                    break;
            }
        }
    }
    //</editor-fold>

    //<editor-fold desc="Location">

    @Override
    public void showLocationEnableDialog(Boolean enabled) {
        EnableLocationDialogFragment dialog = new EnableLocationDialogFragment();
        dialog.setPresenter(presenter);
        dialog.setEnabled(enabled);
        dialog.show(getFragmentManager(), "dialog_fragment_location");
    }

    @Override
    public void notifyLocationAvailable(boolean isAvailable) {
        if (isAvailable)
            makeMessage("Location obtained");
    }


    @Override
    public void showTurnGpsOnDialog() {
        EnableGpsInSettingsDialogFragment dialog = new EnableGpsInSettingsDialogFragment();
        dialog.setPresenter(presenter);
        dialog.show(getFragmentManager(), "EnableGpsInSettingsDialogFragment");
    }

    //</editor-fold>

    //<editor-fold desc="payments list">
    @Override
    public void appendToList(DailyPaymentsVO payments) {
        listFragment.appendToList(payments);
    }

    @Override
    public int getListCount() {
        return listFragment.getListCount();
    }

    @Override
    public void scrollTo(DailyPaymentsVO dailyPaymentsVO) {
        listFragment.scrollTo(dailyPaymentsVO);
    }

    @Override
    public void deletePaymentFromList(PaymentVO paymentVO, DailyPaymentsVO dailyPaymentsVO) {
        listFragment.deletePaymentFromList(paymentVO, dailyPaymentsVO);
    }

    @Override
    public void clearList() {
        listFragment.clearList();
    }

    @Override
    public void pinNoPaymentsMessage(boolean show) {
        listFragment.pinNoPaymentsMessage(show);
    }

    @Override
    public void setSelectionModOn(String message, SelectionType selectionType) {
        listFragment.setSelectionModOn(message, selectionType);
        getSupportActionBar().hide();
    }

    @Override
    public void setSelectionModOff() {
        listFragment.setSelectionModOff();
        getSupportActionBar().show();
    }
    //</editor-fold>

    //<editor-fold desc="Account">
    @Override
    public void inflateAccounts(ArrayList<AccountVO> accounts) {
        ArrayList<String> accountsNames = new ArrayList<>(Stream.of(accounts).map(AccountVO::getAccountName).collect(Collectors.toList()));
        accountsNames.add(getResources().getString(R.string.add_account));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, accountsNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != accountsNames.size() - 1) {
                    presenter.onAccountSelected(accountsNames.get(position));
                } else {
                    selectAccount(presenter.getCurrentAccount().getAccountName());
                    presenter.onAddNewAccountPressed();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    @Override
    public void selectAccount(String accountName) {
        for (int i = 0; i < spinner.getAdapter().getCount(); i++)
            if (spinner.getAdapter().getItem(i).toString().equals(accountName)) {
                spinner.setSelection(i);
                break;
            }
    }

    @Override
    public void showAccountInformation(String accountName, String totalIncome, String
            totalExpense, String summary) {
        AccountDetailsDialogFragment dialog = new AccountDetailsDialogFragment();
        AccountDetailsDialogFragment.setActivity(this);
        dialog.setSummary(summary);
        dialog.setTotalIncome(totalIncome);
        dialog.setTotalExpense(totalExpense);
        dialog.show(getFragmentManager(), "account_details_dialog");
    }

    @Override
    public void showAddNewAccountDialog(Bundle bundle) {
        AddAccountDialogFragment addAccountDialogFragment = new AddAccountDialogFragment();
        addAccountDialogFragment.setPresenter(presenter);
        addAccountDialogFragment.show(getFragmentManager(), "add_account_dialog_fragment");
    }

    @Override
    public void showDeleteAccountDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.main_page_activity_delete_account_message)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(R.string.main_page_activity_delete_account_warning)
                                        .setPositiveButton("Delete anyway",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        presenter.onDeleteCurrentAccountPressed();
                                                        dialog.cancel();
                                                    }
                                                })
                                        .setNegativeButton("No",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                });
                                AlertDialog alert = builder.create();
                                alert.show();
                                dialog.cancel();
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void setOnPurseMod() {
        listFragment.setPurseModOn();
    }

    @Override
    public void setOnGroupCostsMod() {
        listFragment.setGroupCostsModOn();
    }
    //</editor-fold>

    //<editor-fold desc="Payments">
    @Override
    public void showAddPaymentDialog(Bundle savedInstanceState, String
            accountName, PaymentTypeVO
                                             type, ArrayList<CategoryVO> categoriesList, ArrayList<String> payers) {
        AddPaymentDialogFragment addPaymentDialogFragment = new AddPaymentDialogFragment();
        addPaymentDialogFragment.setData(presenter, accountName, type, categoriesList, payers, presenter.isGpsEnabled());
        addPaymentDialogFragment.show(getFragmentManager(), "add_payment_dialog");
    }

    public void showDeletePaymentDialog(PaymentVO payment, DailyPaymentsVO dailyPaymentsVO) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Delete payment?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.onDeletePaymentPressed(payment, dailyPaymentsVO);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
    //</editor-fold>

    //<editor-fold desc="Statistics">
    @Override
    public void showCurrenciesVariantsDialog(List<CurrencyVO> currencies) {
        CurrenciesVariantsDialogFragment dialog = new CurrenciesVariantsDialogFragment();
        dialog.setActivity(this);
        dialog.setCurrencies(currencies);
        dialog.show(getFragmentManager(), "currencies_variants_dialog");
    }

    @Override
    public void showStatistics(HashMap<String, Float> expensesRatios,
                               HashMap<String, Double> expensesCategoriesValues,
                               HashMap<String, Float> incomesRatios,
                               HashMap<String, Double> incomesCategoriesValues,
                               AccountVO account,
                               CurrencyVO currency) {
        StatisticsDialogFragment statisticsDialogFragment = new StatisticsDialogFragment();
        statisticsDialogFragment.setData(expensesRatios,
                expensesCategoriesValues,
                incomesRatios,
                incomesCategoriesValues,
                account,
                currency);
        statisticsDialogFragment.show(getSupportFragmentManager(), "Statistics_dialog_fragment");
    }
    //</editor-fold>

    //<editor-fold desc="Calculate debts">
    @Override
    public void showIfPaymentsAreDistributedEvenlyDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.if_payments_are_distributed_evenly_dialog)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.onEvenlyDistributionPressed(true);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.onEvenlyDistributionPressed(false);
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void showDebtsResultDialog(List<Obligation> obligations) {
        String result = "";
        for (Obligation o : obligations) {
            String correctForm = " owes ";
            String debtor = o.getDebtor();
            String creditor = o.getCreditor();
            if (debtor.length() == 0) {
                debtor = "You";
                correctForm = " owe ";
            } else if (creditor.length() == 0)
                creditor = "you";
            result += debtor + correctForm + o.getAmount() + " " + o.getCurrency() + " to " + creditor + ";\n";
        }
        if (result.length() == 0) {
            makeMessage("No debts!");
            return;
        }
        DebtsResultDialogFragment dialog = new DebtsResultDialogFragment();
        dialog.setData(result);
        dialog.show(getFragmentManager(),"dialog_debts_result");
    }

    //</editor-fold>
}
