package com.aeolus.cashcounter.View;

import android.app.Activity;
import android.os.Bundle;

import com.aeolus.cashcounter.Presenter.BL.Obligation;
import com.aeolus.cashcounter.Presenter.IMainPagePresenter;
import com.aeolus.cashcounter.Presenter.VO.AccountVO;
import com.aeolus.cashcounter.Presenter.VO.CategoryVO;
import com.aeolus.cashcounter.Presenter.VO.CurrencyVO;
import com.aeolus.cashcounter.Presenter.VO.DailyPaymentsVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentTypeVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentVO;
import com.aeolus.cashcounter.Presenter.VO.SelectionType;

import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Yuriy on 06.05.2017.
 */

public interface IView {

    void inflateAccounts(ArrayList<AccountVO> accounts);

    void selectAccount(String accountname);

    void showAddPaymentDialog(Bundle savedInstanceState, String accountName, PaymentTypeVO type, ArrayList<CategoryVO> categories, ArrayList<String> payers);

    void showAddNewAccountDialog(Bundle bundle);

    void appendToList(DailyPaymentsVO payments);

    void clearList();

    void makeMessage(String message);

    Activity getActivity();

    void deletePaymentFromList(PaymentVO paymentVO, DailyPaymentsVO dailyPaymentsVO);

    int getListCount();

    void scrollTo(DailyPaymentsVO dailyPaymentsVO);

    void showAccountInformation(String accountName, String totalIncome, String totalExpense, String Summary);

    void notifyLocationAvailable(boolean isAvailable);

    void showIfPaymentsAreDistributedEvenlyDialog();

    void setSelectionModOn(String message, SelectionType selectionType);

    void setSelectionModOff();

    void showDebtsResultDialog(List<Obligation> obligations);

    void setOnPurseMod();

    void setOnGroupCostsMod();

    void pinNoPaymentsMessage(boolean show);

    void showStatistics(HashMap<String, Float> expensesRatios,
                        HashMap<String, Double> expensesCategoriesValues,
                        HashMap<String, Float> incomesRatios,
                        HashMap<String, Double> incomesCategoriesValues,
                        AccountVO account,
                        CurrencyVO currency);

    IMainPagePresenter getPresenter();

    void showCurrenciesVariantsDialog(List<CurrencyVO> currencies);

    void showDeleteAccountDialog();

    void showSettingsButtonPopUpMenu();

    void showAbout();

    void showNotificationsSettingsDialog( Boolean enabled, LocalTime time);

    void showTurnGpsOnDialog();

    void showLocationEnableDialog(Boolean enabled);

}
