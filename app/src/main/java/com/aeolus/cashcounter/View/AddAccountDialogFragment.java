package com.aeolus.cashcounter.View;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.aeolus.cashcounter.Presenter.IMainPagePresenter;
import com.aeolus.cashcounter.Presenter.VO.AccountTypeVO;
import com.aeolus.cashcounter.Presenter.VO.AccountVO;
import com.aeolus.cashcounter.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yuriy on 10.05.2017.
 */

public class AddAccountDialogFragment extends DialogFragment implements View.OnClickListener {

    //<editor-fold desc="Fields">
    private static IMainPagePresenter presenter;
    private static final String ACCOUNT_NAME_KEY = "add_account_dialog_account_name";
    private static final String PURSE_KEY = "add_account_dialog_purse";
    //</editor-fold>

    //<editor-fold desc="Bindings">
    @BindView(R.id.add_account_dialog_account_name_et)
    EditText accountNameET;

    @BindView(R.id.add_account_dialog_ok_button)
    Button okButton;

    @BindView(R.id.add_account_dialog_type_purse)
    RadioButton purseRB;

    @BindView(R.id.add_account_dialog_type_group_costs)
    RadioButton groupCostsRB;
    //</editor-fold>

    public void setPresenter(IMainPagePresenter presenter) {
        AddAccountDialogFragment.presenter = presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        View view = inflater.inflate(R.layout.dialog_fragment_add_account, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        ButterKnife.bind(this, view);
        okButton.setOnClickListener(this);
        purseRB.setText(AccountTypeVO.PERSONAL.toString().toLowerCase());
        groupCostsRB.setText(AccountTypeVO.GROUP.toString().toLowerCase());
        purseRB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                purseRB.setChecked(true);
                groupCostsRB.setChecked(false);
            }
        });
        groupCostsRB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                purseRB.setChecked(false);
                groupCostsRB.setChecked(true);
            }
        });
        purseRB.performClick();
        if (bundle != null) {
            accountNameET.setText(bundle.getString(ACCOUNT_NAME_KEY));
            if (bundle.getBoolean(PURSE_KEY))
                purseRB.performClick();
            else
                groupCostsRB.performClick();
        }
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(ACCOUNT_NAME_KEY, accountNameET.getText().toString());
        outState.putBoolean(PURSE_KEY, purseRB.isChecked());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_account_dialog_ok_button:
                if (accountNameET.getText().length() == 0) {
                    Toast.makeText(getActivity(), R.string.add_payment_dialog_empty_fields_message, Toast.LENGTH_SHORT).show();
                    break;
                }
                if (accountNameET.getText().equals(presenter.getActivity().getResources().getString(R.string.add_account))) {
                    Toast.makeText(getActivity(), R.string.add_payment_dialog_invalid_name_massage, Toast.LENGTH_SHORT).show();
                    break;
                }
                dismiss();
                presenter.onNewAccountAdded(
                        new AccountVO(
                                accountNameET.getText().toString(),
                                purseRB.isChecked() ? AccountTypeVO.PERSONAL : AccountTypeVO.GROUP)
                );
                break;
            default:
                break;

        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

}
