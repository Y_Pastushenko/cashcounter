package com.aeolus.cashcounter.View;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.aeolus.cashcounter.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yuriy on 10.05.2017.
 */

public class DebtsResultDialogFragment extends DialogFragment {

    private static String result;

    @BindView(R.id.dialog_debts_result_tv)
    TextView resultTV;

    public void setData(String result) {
        DebtsResultDialogFragment.result = result;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_debts_result, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        ButterKnife.bind(this, view);
        resultTV.setText(result);
        return view;
    }
}
