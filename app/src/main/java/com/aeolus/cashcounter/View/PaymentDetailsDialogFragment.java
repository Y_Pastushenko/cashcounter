package com.aeolus.cashcounter.View;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aeolus.cashcounter.Presenter.IMainPagePresenter;
import com.aeolus.cashcounter.Presenter.VO.PaymentTypeVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentVO;
import com.aeolus.cashcounter.R;
import com.google.android.gms.maps.model.LatLng;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yuriy on 10.05.2017.
 */

public class PaymentDetailsDialogFragment extends DialogFragment {
    private static PaymentVO paymentVO;
    private static IMainPagePresenter presenter;

    @BindView(R.id.dialog_payment_details_account_name_tv)
    TextView accountNameTV;

    @BindView(R.id.dialog_payment_details_payer_name_tv)
    TextView payerNameTV;

    @BindView(R.id.dialog_payment_details_amount_tv)
    TextView amountTV;

    @BindView(R.id.dialog_payment_details_currency_tv)
    TextView currencyTV;

    @BindView(R.id.dialog_payment_details_category_tv)
    TextView categoryTV;

    @BindView(R.id.dialog_payment_details_comment_tv)
    TextView commentTV;

    @BindView(R.id.dialog_payment_details_time_tv)
    TextView timeTV;

    @BindView(R.id.dialog_payment_details_date_tv)
    TextView dateTV;

    @BindView(R.id.dialog_payment_details_location_divider)
    View locationDivider;

    @BindView(R.id.dialog_payment_details_location_linear_layout)
    LinearLayout locationLayout;

    @BindView(R.id.dialog_payment_details_show_on_map_button)
    Button showOnMapButton;

    public void setData(PaymentVO payment, IMainPagePresenter presenter) {
        PaymentDetailsDialogFragment.paymentVO = payment;
        PaymentDetailsDialogFragment.presenter = presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_payment_details, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        ButterKnife.bind(this, view);
        String str;
        accountNameTV.setText(paymentVO.getAccount());
        payerNameTV.setText(paymentVO.getPayerName().length() == 0 ? "You" : paymentVO.getPayerName());
        if (paymentVO.getType().equals(PaymentTypeVO.INCOME)) {
            amountTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorIncome));
            str = "+" + paymentVO.getAmount();
        } else {
            amountTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorExpense));
            str = "-" + paymentVO.getAmount();
        }
        amountTV.setText(str);
        currencyTV.setText(paymentVO.getCurrency().toString());
        categoryTV.setText(paymentVO.getCategory());
        commentTV.setText(paymentVO.getComment());
        timeTV.setText(paymentVO.getDt().toLocalTime().getHourOfDay() + ":" + paymentVO.getDt().toLocalTime().getMinuteOfHour());
        dateTV.setText(paymentVO.getDt().toLocalDate().toString());
        if (paymentVO.getLatitude() == -1.0 || paymentVO.getLongitude() == -1.0) {
            locationDivider.setVisibility(View.GONE);
            locationLayout.setVisibility(View.GONE);
        }
        showOnMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onShowPaymentOnMapPressed(new LatLng(paymentVO.getLatitude(),paymentVO.getLongitude()));
            }
        });
        return view;
    }
}
