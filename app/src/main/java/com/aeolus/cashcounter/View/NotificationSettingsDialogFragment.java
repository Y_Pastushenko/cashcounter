package com.aeolus.cashcounter.View;

import android.app.DialogFragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.aeolus.cashcounter.Presenter.IMainPagePresenter;
import com.aeolus.cashcounter.R;

import org.joda.time.LocalTime;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Yuriy on 01.10.2017.
 */

public class NotificationSettingsDialogFragment extends DialogFragment {

    @BindView(R.id.dialog_fragment_notifications_manager_switch)
    Switch enabledSwitch;
    @BindView(R.id.dialog_fragment_notifications_manager_time_tv)
    TextView timePickerText;
    @BindView(R.id.dialog_fragment_notifications_manager_timePicker)
    TimePicker timePicker;
    @BindView(R.id.dialog_fragment_notifications_change_time_button)
    Button changeTimeButton;
    @BindView(R.id.dialog_fragment_notifications_time_chosen_button)
    Button timeChosenButton;

    private static IMainPagePresenter presenter;
    LocalTime time;
    boolean enabled;

    private static final String ENABLED_KEY = "notification_settings_dialog_fragment_enabled_key";

    public static void setPresenter(IMainPagePresenter presenter) {
        NotificationSettingsDialogFragment.presenter = presenter;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_notifications_settings, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        ButterKnife.bind(this, view);
        timePicker.setIs24HourView(true);
        timePicker.setVisibility(View.GONE);
        timeChosenButton.setVisibility(View.GONE);
        changeTimeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeTimeButton.setVisibility(View.GONE);
                timePickerText.setVisibility(View.GONE);
                enabledSwitch.setVisibility(View.GONE);
                timePicker.setVisibility(View.VISIBLE);
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    timePicker.setHour(time.getHourOfDay());
                else
                    timePicker.setCurrentHour(time.getHourOfDay());
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    timePicker.setMinute(time.getMinuteOfHour());
                else
                    timePicker.setCurrentMinute(time.getMinuteOfHour());
                timeChosenButton.setVisibility(View.VISIBLE);
                enabledSwitch.setChecked(false);
            }
        });
        timeChosenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                time = new LocalTime(
                        (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ?
                                timePicker.getHour() :
                                timePicker.getCurrentHour(),
                        (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ?
                                timePicker.getMinute() :
                                timePicker.getCurrentMinute());
                timePicker.setVisibility(View.GONE);
                timeChosenButton.setVisibility(View.GONE);
                changeTimeButton.setVisibility(View.VISIBLE);
                timePickerText.setVisibility(View.VISIBLE);
                enabledSwitch.setVisibility(View.VISIBLE);
                updateTimeText();
            }
        });
        if (enabled)
            enabledSwitch.setChecked(true);
        updateTimeText();
        enabledSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    presenter.onEnableNotificationsPressed(time);
                else
                    presenter.onDisableNotificationsPressed();
            }
        });
        return view;
    }

    void updateTimeText(){
        timePickerText.setText(
                getResources().getString(R.string.dialog_fragment_notifications_manager_time_tv_text)
                        + " " + new LocalTime(time).toString("HH:mm"));
    }

}
