package com.aeolus.cashcounter.Model.DTO;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.aeolus.cashcounter.Model.Database.DBSchema;
import com.aeolus.cashcounter.Presenter.VO.CurrencyVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentTypeVO;
import com.aeolus.cashcounter.Presenter.VO.PaymentVO;

import org.joda.time.DateTime;

/**
 * Created by Yuriy on 25/04/2017.
 */

@Entity(tableName = DBSchema.PaymentsTable.NAME)
public class PaymentDTO implements Comparable<PaymentDTO>, Cloneable, Parcelable {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = DBSchema.PaymentsTable.Columns.ACCOUNT)
    private String accountName;

    @ColumnInfo(name = DBSchema.PaymentsTable.Columns.PERSON_ID)
    private String payerName;

    @ColumnInfo(name = DBSchema.PaymentsTable.Columns.TYPE)
    private PaymentTypeDTO type;

    @ColumnInfo(name = DBSchema.PaymentsTable.Columns.DATE_TIME)
    private DateTime dateTime;

    @ColumnInfo(name = DBSchema.PaymentsTable.Columns.LATITUDE)
    private double latitude;

    @ColumnInfo(name = DBSchema.PaymentsTable.Columns.LONGITUDE)
    private double longitude;

    @ColumnInfo(name = DBSchema.PaymentsTable.Columns.CURRENCY)
    private CurrencyDTO currency;

    @ColumnInfo(name = DBSchema.PaymentsTable.Columns.AMOUNT)
    private double amount;

    @ColumnInfo(name = DBSchema.PaymentsTable.Columns.CATEGORY)
    private String category;

    @ColumnInfo(name = DBSchema.PaymentsTable.Columns.COMMENT)
    private String comment;

    public PaymentDTO() {
        type = null;
        dateTime = null;
        category = null;
        comment = null;
    }

    @Ignore
    public PaymentDTO(String accountName, String payerName, PaymentTypeDTO type, DateTime dateTime, double latitude, double longitude, CurrencyDTO currency, double amount, String category, String comment) {
        this.accountName = accountName;
        this.payerName = payerName;
        this.type = type;
        this.dateTime = dateTime;
        this.latitude = latitude;
        this.longitude = longitude;
        this.currency = currency;
        this.amount = amount;
        this.category = category;
        this.comment = comment;
    }

    @Ignore
    public PaymentDTO(PaymentForSerializing p) {
        this.accountName = p.getAccountName();
        this.payerName = p.getPayerName();
        this.type = PaymentTypeDTO.valueOf(p.getType());
        this.dateTime = new DateTime(p.getDateTime());
        this.latitude = p.getLatitude();
        this.longitude = p.getLongitude();
        this.currency = CurrencyDTO.valueOf(p.getCurrency());
        this.amount = p.getAmount();
        this.category = p.getCategory();
        this.comment = p.getComment();
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public PaymentTypeDTO getType() {
        return type;
    }

    public void setType(PaymentTypeDTO type) {
        this.type = type;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(DateTime dateTime) {
        this.dateTime = dateTime;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public CurrencyDTO getCurrency() {
        return currency;
    }

    public void setCurrency(CurrencyDTO currency) {
        this.currency = currency;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Ignore
    @Override
    public int compareTo(PaymentDTO p) {
        long left = this.getDateTime().getMillis();
        long right = p.getDateTime().getMillis();
        if (left == right) return 0;
        else return left > right ? 1 : -1;
    }

    @Ignore
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Ignore
    public PaymentForSerializing getPaymentForSerializing() {
        return new PaymentForSerializing(accountName, payerName, type.toString(), dateTime.getMillis(), latitude, longitude, currency.toString(), amount, category, comment);
    }

    @Ignore
    public PaymentDTO(Parcel in) {
        id = in.readLong();
        accountName = in.readString();
        payerName = in.readString();
        amount = in.readDouble();
        currency = (CurrencyDTO) in.readSerializable();
        type = (PaymentTypeDTO) in.readSerializable();
        category = in.readString();
        comment = in.readString();
        dateTime = new DateTime(in.readLong());
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    @Ignore
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        public PaymentVO createFromParcel(Parcel in) {
            return new PaymentVO(in);
        }

        public PaymentVO[] newArray(int size) {
            return new PaymentVO[size];
        }
    };

    @Ignore
    @Override
    public int describeContents() {
        return 0;
    }

    @Ignore
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(accountName);
        dest.writeString(payerName);
        dest.writeDouble(amount);
        dest.writeSerializable(currency);
        dest.writeSerializable(type);
        dest.writeString(category);
        dest.writeString(comment);
        dest.writeLong(dateTime.getMillis());
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }
}