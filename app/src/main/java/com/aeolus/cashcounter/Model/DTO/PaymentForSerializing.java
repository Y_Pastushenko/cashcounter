package com.aeolus.cashcounter.Model.DTO;

/**
 * Created by Yuriy on 02.10.2017.
 */

public class PaymentForSerializing {
    private String accountName;
    private String payerName;
    private String type;
    private long dateTime;
    private double latitude;
    private double longitude;
    private String currency;
    private double amount;
    private String category;
    private String comment;

    public PaymentForSerializing(String accountName, String payerName, String type, long dateTime, double latitude, double longitude, String currency, double amount, String category, String comment) {
        this.accountName = accountName;
        this.payerName = payerName;
        this.type = type;
        this.dateTime = dateTime;
        this.latitude = latitude;
        this.longitude = longitude;
        this.currency = currency;
        this.amount = amount;
        this.category = category;
        this.comment = comment;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getPayerName() {
        return payerName;
    }

    public void setPayerName(String payerName) {
        this.payerName = payerName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
