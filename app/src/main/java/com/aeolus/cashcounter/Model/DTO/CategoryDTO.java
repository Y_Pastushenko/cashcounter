package com.aeolus.cashcounter.Model.DTO;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.aeolus.cashcounter.Model.Database.DBSchema;

/**
 * Created by Yuriy on 06.05.2017.
 */

@Entity(tableName = DBSchema.CategoriesTable.NAME)
public class CategoryDTO {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = DBSchema.CategoriesTable.Columns.CATEGORY_NAME)
    private String categoryName;

    @ColumnInfo(name = DBSchema.CategoriesTable.Columns.CATEGORY_TYPE)
    private PaymentTypeDTO type;

    @ColumnInfo(name = DBSchema.CategoriesTable.Columns.CATEGORY_ACCOUNT)
    private String account;

    public CategoryDTO() {
    }

    @Ignore
    public CategoryDTO(String account, String categoryName, PaymentTypeDTO type) {
        this.categoryName = categoryName;
        this.type = type;
        this.account = account;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public PaymentTypeDTO getType() {
        return type;
    }

    public void setType(PaymentTypeDTO type) {
        this.type = type;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
