package com.aeolus.cashcounter.Model.DTO;

/**
 * Created by Yuriy on 11/05/2017.
 */

public enum PaymentTypeDTO {
    INCOME, EXPENSE
}
