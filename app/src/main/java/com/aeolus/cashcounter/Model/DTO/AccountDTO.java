package com.aeolus.cashcounter.Model.DTO;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import com.aeolus.cashcounter.Model.Database.DBSchema;

/**
 * Created by Yuriy on 06.05.2017.
 */

@Entity(tableName = DBSchema.AccountTable.NAME)
public class AccountDTO {

    @PrimaryKey
    @ColumnInfo(name = DBSchema.AccountTable.Columns.ACCOUNT_NAME)
    private String accountName;

    @ColumnInfo(name = DBSchema.AccountTable.Columns.ACCOUNT_TYPE)
    private AccountTypeDTO accountType;

    public AccountDTO() {
    }

    @Ignore
    public AccountDTO(String accountName, AccountTypeDTO accountType) {
        this.accountName = accountName;
        this.accountType = accountType;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public AccountTypeDTO getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountTypeDTO accountType) {
        this.accountType = accountType;
    }
}
