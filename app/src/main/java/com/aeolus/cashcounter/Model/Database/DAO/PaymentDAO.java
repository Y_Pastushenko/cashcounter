package com.aeolus.cashcounter.Model.Database.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.aeolus.cashcounter.Model.DTO.PaymentDTO;
import com.aeolus.cashcounter.Model.Database.DBSchema;

import java.util.List;

/**
 * Created by Yuriy on 11.07.2017.
 */

@Dao
public interface PaymentDAO {

    @Query("SELECT * FROM " +
            DBSchema.PaymentsTable.NAME + " WHERE " + DBSchema.PaymentsTable.Columns.ACCOUNT + " = :accountName" +
            " ORDER BY " + DBSchema.PaymentsTable.Columns.DATE_TIME + " DESC")
    PaymentDTO[] getAll(String accountName);

    @Query("SELECT * FROM " +
            DBSchema.PaymentsTable.NAME +
            " WHERE " +
            DBSchema.PaymentsTable.Columns.DATE_TIME +
            " BETWEEN :minDateTimeInMs AND :maxDateTimeInMs ORDER BY " +
            DBSchema.PaymentsTable.Columns.DATE_TIME + " ASC")
    List<PaymentDTO> getInDateTimeRange(long minDateTimeInMs, long maxDateTimeInMs);

    @Query("SELECT * FROM " +
            DBSchema.PaymentsTable.NAME +
            " WHERE id = :id")
    PaymentDTO getPaymentById(long id);

    @Query("DELETE FROM " +
            DBSchema.PaymentsTable.NAME + " WHERE " + DBSchema.PaymentsTable.Columns.ACCOUNT + " = :accountName")
    void deletePayments (String accountName);

    @Insert
    void insert(PaymentDTO... payments);

    @Delete
    void delete(PaymentDTO payments);

}
