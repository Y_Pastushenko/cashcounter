package com.aeolus.cashcounter.Model;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;

import com.aeolus.cashcounter.Model.DTO.CategoryDTO;
import com.aeolus.cashcounter.Model.DTO.PaymentTypeDTO;
import com.aeolus.cashcounter.Model.DTO.AccountDTO;
import com.aeolus.cashcounter.Model.DTO.PaymentDTO;
import com.aeolus.cashcounter.Model.Database.DBManager;
import com.aeolus.cashcounter.Model.Net.NetManager;
import com.aeolus.cashcounter.Presenter.IMainPagePresenter;

import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observable;

/**
 * Created by Yuriy on 25/04/2017.
 */

public class DataManager implements IDataManger {
    private static IDBManager dbManager = null;
    private static INetManager netManager = null;
    private static final DataManager dataManager = new DataManager();
    private IMainPagePresenter presenter;
    private static final String SHARED_PREFERENCES_LAST_ACCOUNT_NAME = "shared_preferences_last_account_name";
    private static final String SHARED_PREFERENCES_NOTIFICATION_TIME = "shared_preferences_notification_time";
    private static final String SHARED_PREFERENCES_LOCATION_STATUS = "shared_preferences_location_status";
    private LocationManager locationManager;
    private LocationListener locationListener;

    private DataManager() {
    }

    public static DataManager getInstance(Context context) {
        if (dbManager == null)
            dbManager = DBManager.getInstance(context);
        netManager = new NetManager();
        return dataManager;
    }

    private void initLocationService() {
        locationManager = (LocationManager) presenter.getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                presenter.onLocationUpdated(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
    }

    @Override
    public Observable<PaymentDTO> getPayments(String accountName) {
        return dbManager.getPayments(accountName);
    }

    @Override
    public Observable<ArrayList<CategoryDTO>> getCategories(AccountDTO account, PaymentTypeDTO type) {
        return dbManager.getCategories(account, type);
    }

    @Override
    public Observable<ArrayList<AccountDTO>> getAccounts() {
        return dbManager.getAccounts();
    }

    @Override
    public void saveAccount(AccountDTO account) {
        dbManager.saveAccount(account);
    }

    @Override
    public void saveLastAccountName(String value) {
        SharedPreferences sharedPref = presenter.getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SHARED_PREFERENCES_LAST_ACCOUNT_NAME, value);
        editor.apply();
    }

    @Override
    public String getLastAccountName() {
        SharedPreferences sharedPref = presenter.getActivity().getPreferences(Context.MODE_PRIVATE);
        return sharedPref.getString(SHARED_PREFERENCES_LAST_ACCOUNT_NAME, "");
    }

    @Override
    public void saveNotificationTime(LocalTime time) {
        SharedPreferences sharedPref = presenter.getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(SHARED_PREFERENCES_NOTIFICATION_TIME, time.toString());
        editor.apply();
    }

    @Override
    public LocalTime getNotificationTime() {
        SharedPreferences sharedPref = presenter.getActivity().getPreferences(Context.MODE_PRIVATE);
        return new LocalTime(sharedPref.getString(SHARED_PREFERENCES_NOTIFICATION_TIME, new LocalTime(0, 0).toString()));
    }

    @Override
    public void saveLocationStatus(Boolean enabled) {
        SharedPreferences sharedPref = presenter.getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(SHARED_PREFERENCES_LOCATION_STATUS, enabled);
        editor.apply();
    }

    @Override
    public boolean getLocationStatus() {
        SharedPreferences sharedPref = presenter.getActivity().getPreferences(Context.MODE_PRIVATE);
        return sharedPref.getBoolean(SHARED_PREFERENCES_LOCATION_STATUS, false);
    }

    @Override
    public void saveCategory(CategoryDTO category) {
        dbManager.saveCategory(category);
    }

    @Override
    public void savePayment(PaymentDTO payment) {
        dbManager.savePayment(payment);
    }

    @Override
    public void deleteAccount(AccountDTO accountDTO) {
        dbManager.deleteAccount(accountDTO);
    }

    @Override
    public void deleteCategory(CategoryDTO categoryDTO) {
        dbManager.deleteCategory(categoryDTO);
    }

    @Override
    public void deleteCategoriesByAccount(AccountDTO account) {
        dbManager.deleteCategoriesByAccount(account.getAccountName());
    }

    @Override
    public void deletePayment(PaymentDTO paymentDTO) {
        dbManager.deletePayment(paymentDTO);
    }

    @Override
    public void deletePayments(AccountDTO accountDTO) {
        dbManager.deletePayments(accountDTO);
    }

    @Override
    public void setPresenter(IMainPagePresenter presenter) {
        this.presenter = presenter;
        initLocationService();
    }

    @Override
    public void enableLocation(boolean enable) {
        if (enable) {
            if (ContextCompat.checkSelfPermission(presenter.getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                presenter.onLocationPermissionCheckFailed();
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        } else
            locationManager.removeUpdates(locationListener);
    }

    @Override
    public Observable<HashMap<String, Double>> getExchangeRates(NetManager.RateType type) {
        return netManager.getExchangeRates(type);
    }
}
