package com.aeolus.cashcounter.Model;

import com.aeolus.cashcounter.Model.Net.NetManager;

import java.util.HashMap;

import io.reactivex.Observable;

/**
 * Created by Yuriy on 30.09.2017.
 */

public interface INetManager {
    Observable<HashMap<String, Double>> getExchangeRates(NetManager.RateType type);
}
