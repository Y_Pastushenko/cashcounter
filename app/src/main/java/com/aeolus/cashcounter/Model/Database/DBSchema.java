package com.aeolus.cashcounter.Model.Database;

/**
 * Created by Yuriy on 11.07.2017.
 */

public class DBSchema {
    public static final String NAME = "CashCounterDB";

    public static final class AccountTable {
        public static final String NAME = "accounts";

        public static final class Columns {
            public static final String ACCOUNT_NAME = "name";
            public static final String ACCOUNT_TYPE = "account_type";
        }
    }

    public static final class CategoriesTable {
        public static final String NAME = "categories";

        public static final class Columns {
            public static final String CATEGORY_NAME = "name";
            public static final String CATEGORY_TYPE = "type";
            public static final String CATEGORY_ACCOUNT = "account";
        }
    }

    public static final class PaymentsTable {
        public static final String NAME = "payments";

        public static final class Columns {
            public static final String ACCOUNT = "name";
            public static final String PERSON_ID = "persons_id";
            public static final String TYPE = "type";
            public static final String DATE_TIME = "dateTime";
            public static final String LONGITUDE = "longitude";
            public static final String LATITUDE = "latitude";
            public static final String CURRENCY = "currency";
            public static final String AMOUNT = "amount";
            public static final String CATEGORY = "category";
            public static final String COMMENT = "comment";
        }
    }
}
