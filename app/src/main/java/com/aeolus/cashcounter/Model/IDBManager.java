package com.aeolus.cashcounter.Model;

import com.aeolus.cashcounter.Model.DTO.AccountDTO;
import com.aeolus.cashcounter.Model.DTO.CategoryDTO;
import com.aeolus.cashcounter.Model.DTO.PaymentDTO;
import com.aeolus.cashcounter.Model.DTO.PaymentTypeDTO;

import java.util.ArrayList;

import io.reactivex.Observable;

/**
 * Created by Yuriy on 11.07.2017.
 */

public interface IDBManager {

    void savePayment(PaymentDTO payment);

    Observable<PaymentDTO> getPayments(String accountName);

    void deletePayment(PaymentDTO paymentDTO);

    void saveAccount(AccountDTO account);

    Observable<ArrayList<AccountDTO>> getAccounts();

    void deleteAccount(AccountDTO accountDTO);

    void saveCategory(CategoryDTO category);

    Observable<ArrayList<CategoryDTO>> getCategories(AccountDTO account,PaymentTypeDTO type);

    void deleteCategory(CategoryDTO categoryDTO);

    void deleteCategoriesByAccount(String accountName);

    void deletePayments(AccountDTO accountDTO);
}
