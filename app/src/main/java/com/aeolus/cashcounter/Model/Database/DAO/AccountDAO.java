package com.aeolus.cashcounter.Model.Database.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.aeolus.cashcounter.Model.DTO.AccountDTO;
import com.aeolus.cashcounter.Model.Database.DBSchema;

import java.util.List;

/**
 * Created by Yuriy on 11.07.2017.
 */

@Dao
public interface AccountDAO {
    @Query("SELECT * FROM " + DBSchema.AccountTable.NAME)
    List<AccountDTO> getAll();

    @Insert
    void insert(AccountDTO... accounts);

    @Delete
    void delete(AccountDTO account);
}
