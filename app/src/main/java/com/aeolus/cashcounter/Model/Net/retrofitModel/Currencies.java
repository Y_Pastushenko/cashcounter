
package com.aeolus.cashcounter.Model.Net.retrofitModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Currencies {

    @SerializedName("EUR")
    @Expose
    private EUR eUR;
    @SerializedName("USD")
    @Expose
    private USD uSD;
    @SerializedName("RUB")
    @Expose
    private RUB rUB;
    @SerializedName("CHF")
    @Expose
    private CHF cHF;
    @SerializedName("CZK")
    @Expose
    private CZK cZK;
    @SerializedName("GBP")
    @Expose
    private GBP gBP;
    @SerializedName("PLN")
    @Expose
    private PLN pLN;

    public EUR getEUR() {
        return eUR;
    }

    public void setEUR(EUR eUR) {
        this.eUR = eUR;
    }

    public USD getUSD() {
        return uSD;
    }

    public void setUSD(USD uSD) {
        this.uSD = uSD;
    }

    public RUB getRUB() {
        return rUB;
    }

    public void setRUB(RUB rUB) {
        this.rUB = rUB;
    }

    public CHF getCHF() {
        return cHF;
    }

    public void setCHF(CHF cHF) {
        this.cHF = cHF;
    }

    public CZK getCZK() {
        return cZK;
    }

    public void setCZK(CZK cZK) {
        this.cZK = cZK;
    }

    public GBP getGBP() {
        return gBP;
    }

    public void setGBP(GBP gBP) {
        this.gBP = gBP;
    }

    public PLN getPLN() {
        return pLN;
    }

    public void setPLN(PLN pLN) {
        this.pLN = pLN;
    }

}
