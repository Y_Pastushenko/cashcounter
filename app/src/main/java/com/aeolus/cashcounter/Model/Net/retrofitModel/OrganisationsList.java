
package com.aeolus.cashcounter.Model.Net.retrofitModel;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrganisationsList {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("organizations")
    @Expose
    private List<Organization> organizations = null;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Organization> getOrganizations() {
        return organizations;
    }

    public void setOrganizations(List<Organization> organizations) {
        this.organizations = organizations;
    }
}
