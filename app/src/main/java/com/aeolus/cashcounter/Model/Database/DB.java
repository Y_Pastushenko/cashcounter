package com.aeolus.cashcounter.Model.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.aeolus.cashcounter.Model.DTO.AccountDTO;
import com.aeolus.cashcounter.Model.DTO.CategoryDTO;
import com.aeolus.cashcounter.Model.DTO.PaymentDTO;
import com.aeolus.cashcounter.Model.Database.DAO.AccountDAO;
import com.aeolus.cashcounter.Model.Database.DAO.CategoryDAO;
import com.aeolus.cashcounter.Model.Database.DAO.PaymentDAO;

/**
 * Created by Yuriy on 11.07.2017.
 */

@Database(entities = {AccountDTO.class, CategoryDTO.class, PaymentDTO.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class DB extends RoomDatabase {
    public abstract AccountDAO accountDAO();

    public abstract CategoryDAO categoryDAO();

    public abstract PaymentDAO paymentDAO();

}
