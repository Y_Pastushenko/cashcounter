package com.aeolus.cashcounter.Model.Net;

import com.aeolus.cashcounter.Model.Net.retrofitModel.OrganisationsList;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Yuriy on 29.09.2017.
 */

public interface FinanceUaApi {
    @GET("/ru/public/currency-cash.json")
    Call<OrganisationsList> getOrganisationsList();
}
