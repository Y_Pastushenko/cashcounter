package com.aeolus.cashcounter.Model.Database;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.aeolus.cashcounter.Model.DTO.AccountDTO;
import com.aeolus.cashcounter.Model.DTO.CategoryDTO;
import com.aeolus.cashcounter.Model.DTO.PaymentDTO;
import com.aeolus.cashcounter.Model.DTO.PaymentTypeDTO;
import com.aeolus.cashcounter.Model.IDBManager;

import java.util.ArrayList;

import io.reactivex.Observable;

/**
 * Created by Yuriy on 11.07.2017.
 */

public class DBManager implements IDBManager {

    private static DBManager dbManager = null;
    private static DB db;

    private DBManager() {
    }

    public static synchronized DBManager getInstance(Context context) {
        if (dbManager == null) {
            dbManager = new DBManager();
            db = Room.databaseBuilder(context, DB.class, DBSchema.NAME).allowMainThreadQueries().build(); //ToDo bad, very bad
        }
        return dbManager;
    }

    @Override
    public Observable<PaymentDTO> getPayments(String accountName) {
        return Observable.fromArray(db.paymentDAO().getAll(accountName));
    }

    @Override
    public Observable<ArrayList<CategoryDTO>> getCategories(AccountDTO account,PaymentTypeDTO type) {
        return Observable.just(new ArrayList<>(db.categoryDAO().getCategoriesForAccount(account.getAccountName(), type.toString())));
    }

    @Override
    public void deleteAccount(AccountDTO accountDTO) {
        db.accountDAO().delete(accountDTO);
    }

    @Override
    public void deletePayment(PaymentDTO paymentDTO) {
        db.paymentDAO().delete(paymentDTO);
    }

    @Override
    public void deletePayments(AccountDTO accountDTO) {
        db.paymentDAO().deletePayments(accountDTO.getAccountName());
    }

    @Override
    public void deleteCategory(CategoryDTO categoryDTO) {
        db.categoryDAO().delete(categoryDTO);
    }

    @Override
    public void deleteCategoriesByAccount(String accountName) {
        db.categoryDAO().deleteCategoriesByAccount(accountName);
    }

    @Override
    public Observable<ArrayList<AccountDTO>> getAccounts() {
        return Observable.just(new ArrayList<>(db.accountDAO().getAll()));
    }

    @Override
    public void saveAccount(AccountDTO account) {
        db.accountDAO().insert(account);
    }

    @Override
    public void saveCategory(CategoryDTO category) {
        db.categoryDAO().insert(category);
    }

    @Override
    public void savePayment(PaymentDTO payment) {
        db.paymentDAO().insert(payment);
    }
}
