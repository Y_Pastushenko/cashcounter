package com.aeolus.cashcounter.Model.Database.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.aeolus.cashcounter.Model.DTO.CategoryDTO;
import com.aeolus.cashcounter.Model.Database.DBSchema;

import java.util.List;

/**
 * Created by Yuriy on 11.07.2017.
 */

@Dao
public interface CategoryDAO {
    @Query("SELECT * FROM " + DBSchema.CategoriesTable.NAME
            + " WHERE " + DBSchema.CategoriesTable.Columns.CATEGORY_ACCOUNT + " = :account"
            + " AND " + DBSchema.CategoriesTable.Columns.CATEGORY_TYPE + " = :category")
    List<CategoryDTO> getCategoriesForAccount(String account, String category);

    @Query("DELETE FROM " + DBSchema.CategoriesTable.NAME + " WHERE " + DBSchema.PaymentsTable.Columns.ACCOUNT + " = :account")
    void deleteCategoriesByAccount(String account);

    @Insert
    void insert(CategoryDTO... categories);

    @Delete
    void delete(CategoryDTO category);
}
