package com.aeolus.cashcounter.Model.DTO;

import android.support.annotation.NonNull;

import org.joda.time.LocalDate;

import java.util.ArrayList;

/**
 * Created by Yuriy on 08.07.2017.
 */

public class DailyPaymentsDTO implements Comparable<DailyPaymentsDTO> {
    private LocalDate date = null;
    public ArrayList<PaymentDTO> payments = new ArrayList<>();

    public DailyPaymentsDTO() {
    }

    public DailyPaymentsDTO(LocalDate date, ArrayList<PaymentDTO> payments) {
        this.date = date;
        this.payments = payments;
    }

    public void add(PaymentDTO paymentDTO) throws IncorrectDateInsertedException {
        if (date == null)
            date = paymentDTO.getDateTime().toLocalDate();
        if (!date.equals(paymentDTO.getDateTime().toLocalDate()))
            throw new IncorrectDateInsertedException();
        payments.add(paymentDTO);
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public ArrayList<PaymentDTO> getPayments() {
        return payments;
    }

    public void setPayments(ArrayList<PaymentDTO> payments) {
        this.payments = payments;
    }

    @Override
    public int compareTo(@NonNull DailyPaymentsDTO anotherDailyPaymentsDTO) {
        return this.payments.get(0).getDateTime().toLocalDate().compareTo(anotherDailyPaymentsDTO.payments.get(0).getDateTime().toLocalDate());
    }

    public class IncorrectDateInsertedException extends Exception {
    }

    public void clear() {
        date = null;
        payments.clear();
    }
}
