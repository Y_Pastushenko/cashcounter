package com.aeolus.cashcounter.Model.Database;

import android.arch.persistence.room.TypeConverter;

import com.aeolus.cashcounter.Model.DTO.AccountDTO;
import com.aeolus.cashcounter.Model.DTO.AccountTypeDTO;
import com.aeolus.cashcounter.Model.DTO.CurrencyDTO;
import com.aeolus.cashcounter.Model.DTO.PaymentTypeDTO;

import org.joda.time.DateTime;

/**
 * Created by Yuriy on 11.07.2017.
 */

public class Converters {
    @TypeConverter
    public static DateTime fromTimestamp(Long value) {
        return value == null ? null : new DateTime(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(DateTime dateTime) {
        return dateTime == null ? null : dateTime.getMillis();
    }

    @TypeConverter
    public static PaymentTypeDTO PaymentTypeDTOFromString(String value) {
        return PaymentTypeDTO.valueOf(value);
    }

    @TypeConverter
    public static String typeToString(PaymentTypeDTO type) {
        return type.toString();
    }

    @TypeConverter
    public static CurrencyDTO CurrencyDTOFromString(String value) {
        return CurrencyDTO.valueOf(value);
    }

    @TypeConverter
    public static String currencyToString(CurrencyDTO currency) {
        return currency.toString();
    }

    @TypeConverter
    public static AccountTypeDTO fromString(String value) {
        return AccountTypeDTO.valueOf(value);
    }

    @TypeConverter
    public static String accountTypeToString(AccountTypeDTO accountType) {
        return accountType.toString();
    }
}
