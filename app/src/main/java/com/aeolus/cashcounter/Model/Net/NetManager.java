package com.aeolus.cashcounter.Model.Net;

import com.aeolus.cashcounter.Model.DTO.CurrencyDTO;
import com.aeolus.cashcounter.Model.INetManager;
import com.aeolus.cashcounter.Model.Net.retrofitModel.Currencies;
import com.aeolus.cashcounter.Model.Net.retrofitModel.OrganisationsList;
import com.aeolus.cashcounter.Model.Net.retrofitModel.Organization;
import com.annimon.stream.Stream;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Yuriy on 29.09.2017.
 */

public class NetManager implements INetManager{
    private FinanceUaApi financeUaApi;
    private Retrofit retrofit;

    public enum RateType {ASK, BID}

    public FinanceUaApi getApi() {
        if (financeUaApi == null || retrofit == null) {
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl("http://resources.finance.ua/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            financeUaApi = retrofit.create(FinanceUaApi.class);
        }
        return financeUaApi;
    }

    public Observable<HashMap<String, Double>> getExchangeRates(RateType type) {
        return Observable.create(emitter -> {
            FinanceUaApi api = getApi();
            api.getOrganisationsList().enqueue(new Callback<OrganisationsList>() {
                @Override
                public void onResponse(Call<OrganisationsList> call, Response<OrganisationsList> response) {
                    HashMap<String, Double> result = new HashMap<>();
                    OrganisationsList organisationsList = response.body();
                    List<Organization> organizations = organisationsList.getOrganizations();
                    List<Currencies> currenciesList = Stream.of(organizations).map(Organization::getCurrencies).toList();
                    if (type == RateType.ASK) {
                        int nUSD = 0;
                        int nEUR = 0;
                        double usdAskSum = 0;
                        double eurAskSum = 0;
                        for (Currencies c : currenciesList) {
                            if (c.getUSD() != null) {
                                usdAskSum += Double.parseDouble(c.getUSD().getAsk());
                                nUSD++;
                            }
                            if (c.getEUR() != null) {
                                eurAskSum += Double.parseDouble(c.getEUR().getAsk());
                                nEUR++;
                            }
                        }
                        result.put(CurrencyDTO.USD.toString(), usdAskSum / nUSD);
                        result.put(CurrencyDTO.EUR.toString(), eurAskSum / nEUR);
                        emitter.onNext(result);
                    } else if (type == RateType.BID) {
                        int nUSD = 0;
                        int nEUR = 0;
                        double usdBidSum = 0;
                        double eurBidSum = 0;
                        for (Currencies c : currenciesList) {
                            if (c.getUSD() != null) {
                                usdBidSum += Double.parseDouble(c.getUSD().getAsk());
                                nUSD++;
                            }
                            if (c.getEUR() != null) {
                                eurBidSum += Double.parseDouble(c.getEUR().getAsk());
                                nEUR++;
                            }
                        }
                        result.put(CurrencyDTO.USD.toString(), usdBidSum / nUSD);
                        result.put(CurrencyDTO.EUR.toString(), eurBidSum / nEUR);
                        emitter.onNext(result);
                    }
                    emitter.onComplete();
                }

                @Override
                public void onFailure(Call<OrganisationsList> call, Throwable t) {

                }
            });
        });
    }
}
