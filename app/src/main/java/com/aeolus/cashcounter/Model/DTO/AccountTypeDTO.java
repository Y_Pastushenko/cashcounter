package com.aeolus.cashcounter.Model.DTO;

/**
 * Created by Yuriy on 26.09.2017.
 */

public enum AccountTypeDTO {
    PERSONAL, GROUP
}
