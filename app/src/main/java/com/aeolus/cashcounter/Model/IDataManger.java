package com.aeolus.cashcounter.Model;

import com.aeolus.cashcounter.Model.DTO.AccountDTO;
import com.aeolus.cashcounter.Model.DTO.CategoryDTO;
import com.aeolus.cashcounter.Model.DTO.PaymentDTO;
import com.aeolus.cashcounter.Model.DTO.PaymentTypeDTO;
import com.aeolus.cashcounter.Model.Net.NetManager;
import com.aeolus.cashcounter.Presenter.IMainPagePresenter;

import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observable;

/**
 * Created by Yuriy on 25/04/2017.
 */

public interface IDataManger {
    Observable<PaymentDTO> getPayments(String accountName);

    Observable<ArrayList<AccountDTO>> getAccounts();

    Observable<ArrayList<CategoryDTO>> getCategories(AccountDTO account, PaymentTypeDTO type);

    void savePayment(PaymentDTO payment);

    void saveAccount(AccountDTO account);

    void saveCategory(CategoryDTO categoryDTO);

    void deletePayment(PaymentDTO paymentDTO);

    void deletePayments(AccountDTO accountDTO);

    void deleteAccount(AccountDTO accountDTO);

    void deleteCategory(CategoryDTO categoryDTO);

    void deleteCategoriesByAccount(AccountDTO account);

    void setPresenter(IMainPagePresenter presenter);

    void enableLocation(boolean enable);

    Observable<HashMap<String, Double>> getExchangeRates(NetManager.RateType type);

    void saveLastAccountName(String value);

    String getLastAccountName();

    void saveNotificationTime(LocalTime time);

    LocalTime getNotificationTime();

    void saveLocationStatus(Boolean enabled);

    boolean getLocationStatus();

}
