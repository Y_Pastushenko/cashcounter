package com.aeolus.cashcounter.Model.DTO;

/**
 * Created by Yuriy on 07.08.2017.
 */

public enum CurrencyDTO {
    UAH, USD, EUR
}
